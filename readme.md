# 開發注意事項

### 1. 全系統時間格式(LocalDate, LocalDateTime, LocalTime)的傳入格式，統一使用標準ISO格式的String傳入
格式：yyyy-MM-ddTHH:mm:ss.SSSXXX
範例(UTC)：2020-09-25T16:00:00.000Z
範例(GMT+8)：2020-09-25T16:00:00.000+08:00
針對LocalTime的部分，Date可隨意輸入
針對LocalDate的部分，Time可隨意輸入
以上的序列化/反序列化開發不須顧慮，接口完美支持前端輸入

### 2. 關於VO及DTO，若VO和DTO欄位一樣，須個別新增，物件不可共用
- 本模組(internal) VO: 後段 > 前端
- 本模組(internal) DTO: 前端 > 後端
- 外部模組(external) VO: 後端 > 前端
- 外部模組(external) DTO: 後端 > 後端 (FC)

### 4. Generator產生代碼以後，Controller不用放進專案。

### 5. 分支原則

### 6. 關於分頁參數
接PaginationVO時使用@ModelAttribute，不使用@RequestParam

### 7. Mapping使用時機如下
* GET： 取數據
* POST：新增數據
* PATCH：更新數據
* DELETE：刪除數據
* PUT：不使用

### 8. SD架構參考下圖
![structure-sd](structure-sd.jpg)

### 9. 重要！
- 第一層 sectionTypeName = 原來的sectionCode = 第一層-工藝類型
- 第二層 sectionName = 第二層-工藝名稱-使用者自訂
- 第三層 sectionCode = 原來的sectionId = 第三層-工藝代碼(使用者自訂+有流水號)

### 10. Api Url 命名
### 命名形式
- 動作 + 數據 + 數據型態 + 引用參數 舉例如下：
1. getPersonalTaskList (ByTaskCamUid)
2. updatePersonalTask (ByXXX)
3. confirmPersonalTask (ByTaskCamUid)
4. getPersonalTaskListByPage
5. deleteCamTaskByCamUid
6. getPersonalTaskMapByCamUidSet

### RequestMethod
* GET：取數據、取用時新增數據
* POST：新增數據
* PATCH：更新數據
* DELETE:：刪除數據
* PUT：不使用

### 基本要求
* URL動詞不加進行式(ing)
* URL不加時態(ed、ied)
* URL不加冗餘語法(whether、if)
* URL開頭必定為動詞，如 get delete confirm update check
* URL使用詞輟盡量與資料庫相關，舉例：TableName = ProcessSample 則 URL必定包含 ProcessSample，細部請參考下面的範例
* 下拉選單必定為 DropList 結尾
* 分頁必定為Page結尾
* 集合數據必定為List結尾
* 集合數據+分頁必為 ListPage 或 ListByPage結尾
* 可不加BY，若加BY，需要完整命名變數名稱
* 禁止rest風格及path引用 如  api/{xxxx}/2,  api/delete/1 等


### 範例：
| Resource | TableName     | GET                       | POST                                                | PATCH                    | DELETE                   |
| -------- | ------------- | ------------------------- | --------------------------------------------------- | ------------------------ | ------------------------ |
| 單一數據 | processSample | /getProcessSample          | /confirmProcessSample or /saveProcessSample         | /updateProcessSample     | /deleteProcessSample     |
| 集合數據 | processSample | /getProcessSampleList     | /confirmProcessSampleList or /saveProcessSampleList | /updateProcessSampleList | /deleteProcessSampleList |
| 下拉選單 | processSample | /getProcessSampleDropList |                                                     |                          |                          |
| 分頁數據 | processSample | /getProcessSampleListPage |                                                     |                          |                          |

#### GET METHOD傳入參數如下
| DataType | Method Parameter | Method Parameter2 |
| -------- | ---------------- | ----------------- |
| variable | @RequestParam    |                   |
| list     | @PathVariable    | @RequestParam     |
| object   | @ModelAttribute             |                   |

#### DELETE傳入參數如下
| DataType | Method Parameter | Method Parameter2 |
| -------- | ---------------- | ----------------- |
| variable | @RequestParam    |                   |
| list     | @PathVariable    | @RequestParam     |
| object   | @ModelAttribute               |                   |

#### POST傳入參數如下
| DataType | Method Parameter1 | Method Parameter2 |
| -------- | ----------------- | ----------------- |
| variable | @RequestParam     |                   |
| list     | @RequestBody      | @RequestParam     |
| object   | @RequestBody      |                   |

#### PATCH傳入參數如下
| DataType | Method Parameter1 | Method Parameter2 |
| -------- | ----------------- | ----------------- |
| variable | @RequestParam     |                   |
| list     | @RequestBody      | @RequestParam     |
| object   | @RequestBody      |                   |

### 11 相關細節

以下兩點務必注意，否則會造成Swagger顯示問題
- VO或DTO如果有繼承MODEL的情況，使用```@Data``` 一定要加上 ```@EqualsAndHashCode(callSuper = false)```， CallSuper一定要是```false```，不能是```true```
- ApiModel的敘述一定是```@ApiModel(description = "xxx")```而不是```@ApiModel(value = "xxx")```

### 12. Mybatis相關規範
- 使用saveOrUpdateBatch或saveBatch，若發生問題，將iServiceImpl的父類改繼承AbstractServiceImpl
- 需要用到xml, 才使用mapper, 其餘操作(list, getOne, delete, selectOne)請用Service方法
- 使用service更新數據，不需要對下列欄位createId, createDate, updateId, updateDate賦值，未來需要update也直接使用service，不用mapper
- 若可能會將欄位更新成NULL，參考以下這篇 [MyBatis 遇到字段不更新的解決方案](https://cimforce-twtfs/DefaultCollection/MES_Module/_wiki/wikis/MES_Module.wiki?wikiVersion=GBwikiMaster&pagePath=%2F%E5%BE%8C%E7%AB%AF%E9%96%8B%E7%99%BC%2F%E6%8A%80%E8%A1%93%E7%AD%86%E8%A8%98%2FMyBatis%20%E9%81%87%E5%88%B0%E5%AD%97%E6%AE%B5%E4%B8%8D%E6%9B%B4%E6%96%B0%E7%9A%84%E8%A7%A3%E6%B1%BA%E6%96%B9%E6%A1%88)

### 13. 分布式架構事務解決方案
[Seata教學](https://seata.io/zh-cn/docs/overview/what-is-seata.html)  