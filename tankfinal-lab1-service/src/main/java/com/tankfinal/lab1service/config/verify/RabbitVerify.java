package com.tankfinal.lab1service.config.verify;


import cn.hutool.extra.spring.SpringUtil;
import com.rabbitmq.client.ShutdownNotifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

/**
 * 列舉工具類
 *
 * @author Tank.Yang
 * @since 2020-09-08 20:18:44
 */
@Slf4j
public final class RabbitVerify {

    private RabbitVerify() {
    }

    /**
     * 執行初始化檢查
     *
     * @author Tank.Yang
     * @since 2020-09-08 20:18:28
     */
    public static void verify() {
        RabbitTemplate rabbitTemplate = SpringUtil.getBean(RabbitTemplate.class);
        ConnectionFactory connectionFactory = rabbitTemplate.getConnectionFactory();
        try {
            rabbitTemplate.execute(ShutdownNotifier::isOpen);
        } catch (Exception e) {
            log.error("Rabbit MQ服務器 {}:{} 不存在，強制關閉服務！", connectionFactory.getHost(), connectionFactory.getPort());
            throw e;
        }
    }

}
