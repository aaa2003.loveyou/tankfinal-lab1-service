package com.tankfinal.lab1service.config.mybatis.generator.config;

import lombok.Getter;
import lombok.Setter;

/**
 * 代碼自動生成配置類集成
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:32:23
 */
@Getter
@Setter
class GeneratorConfig {

    /**
     * 工程配置
     */
    private Project project;
    /**
     * 全局配置
     */
    private Global global;
    /**
     * 包配置
     */
    private Pack pack;
    /**
     * 數據庫配置
     */
    private Db db;
    /**
     * 文件配置
     */
    private File file;

    /**
     * 數據庫配置
     *
     * @author Tank.Yang
     * @since 2020-06-16 13:32:13
     */
    @Getter
    @Setter
    static class Db {

        /**
         * 驅動名稱
         */
        private String driver;
        /**
         * 連接URL
         */
        private String url;
        /**
         * 數據庫用戶
         */
        private String user;
        /**
         * 數據庫密碼
         */
        private String password;
    }

    /**
     * 文件配置
     *
     * @author Tank.Yang
     * @since 2020-06-16 13:31:43
     */
    @Getter
    @Setter
    static class File {
        /**
         * 文件输出路徑
         */
        private String outPutDir;
        /**
         * 是否覆蓋本地已生成的文件
         */
        private boolean fileOverRide;
    }

    /**
     * 全局配置
     *
     * @author Tank.Yang
     * @since 2020-06-16 13:31:39
     */
    @Getter
    @Setter
    static class Global {

        /**
         * 统一表前缀,例如 sys_
         */
        private String tablePrefix;
        /**
         * 是否生成BaseResultMap,在XML中的返回结果映射
         */
        private boolean genBaseResultMap;
        /**
         * 是否生成所有列映射SQL
         */
        private boolean genBaseColumnList;
        /**
         * 是否给每个實體类生成所有字段的常量變量
         */
        private boolean genEntityColumnConstant;
        /**
         * 生成實體類是否按照Lombok模板生成
         */
        private boolean genEntityLombokModel;

        private boolean genEntityBuilderModel;

    }

    /**
     * 生成代碼包配置
     *
     * @author Tank.Yang
     * @since 2020-06-16 13:30:55
     */
    @Getter
    @Setter
    static class Pack {

        /**
         * 父包名
         */
        private String name;
        /**
         * 控制層包名
         */
        private String controller;
        /**
         * 業務層接口包名
         */
        private String service;
        /**
         * 業務層實現包名
         */
        private String serviceImpl;
        /**
         * 持久層包名
         */
        private String mapper;
        /**
         * 模型層包名
         */
        private String entity;
        /**
         * xml包名
         */
        private String xml;
    }

    /**
     * 工程配置
     *
     * @author Tank.Yang
     * @since 2020-06-16 13:31:30
     */
    @Getter
    @Setter
    static class Project {
        /**
         * 工程名稱
         */
        private String name;
        /**
         * 作者
         */
        private String author;
    }

}
