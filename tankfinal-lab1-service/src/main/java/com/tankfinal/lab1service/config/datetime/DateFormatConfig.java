package com.tankfinal.lab1service.config.datetime;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tankfinal.lab.core.util.DateUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * 全域時間序列化及反序列化器
 *
 * @author Tank.Yang
 * @since 2020-09-24 15:15:42
 */
@Configuration
public class DateFormatConfig {

    @Bean
    @Primary
    public ObjectMapper serializingObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer());
        javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer());

        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
        javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer());
        javaTimeModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, Boolean.FALSE);
        objectMapper.registerModule(javaTimeModule);
        return objectMapper;
    }

    /**
     * LocalDatetime序列化器
     *
     * @author Tank.Yang
     * @since 2020-09-24 10:43:39
     */
    public static class LocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {
        @Override
        public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(
                    value.atZone(ZoneId.systemDefault()).format(DateUtil.Formatter.ZONED_DATE_TIME_OFFSET.getFormatter())
            );
        }
    }

    /**
     * LocalTime序列化器
     *
     * @author Tank.Yang
     * @since 2020-09-24 10:43:39
     */
    public static class LocalTimeSerializer extends JsonSerializer<LocalTime> {
        @Override
        public void serialize(LocalTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(
                    LocalDateTime.of(LocalDate.now(), value)
                            .atZone(ZoneId.systemDefault())
                            .format(DateUtil.Formatter.ZONED_DATE_TIME_OFFSET.getFormatter()));
        }
    }

    /**
     * LocalDate序列化器
     *
     * @author Tank.Yang
     * @since 2020-09-24 10:43:39
     */
    public static class LocalDateSerializer extends JsonSerializer<LocalDate> {
        @Override
        public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            gen.writeString(
                    LocalDateTime.of(value, LocalTime.MIN)
                            .atZone(ZoneId.systemDefault())
                            .format(DateUtil.Formatter.ZONED_DATE_TIME_OFFSET.getFormatter()));
        }
    }

    /**
     * LocalDatetime反序列化器
     *
     * @author Tank.Yang
     * @since 2020-09-24 10:43:39
     */
    public static class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
        @Override
        public LocalDateTime deserialize(JsonParser p, DeserializationContext deserializationContext) throws IOException {
            return DateUtil.parseZonedDateTimeWithFormatter(p.getValueAsString()).withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        }
    }

    /**
     * LocalDate反序列化器
     *
     * @author Tank.Yang
     * @since 2020-09-24 10:43:39
     */
    public static class LocalDateDeserializer extends JsonDeserializer<LocalDate> {
        @Override
        public LocalDate deserialize(JsonParser p, DeserializationContext deserializationContext) throws IOException {
            return DateUtil.parseZonedDateTimeWithFormatter(p.getValueAsString()).withZoneSameInstant(ZoneId.systemDefault()).toLocalDate();
        }
    }

    /**
     * LocalTime反序列化器
     *
     * @author Tank.Yang
     * @since 2020-09-24 10:43:39
     */
    public static class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {
        @Override
        public LocalTime deserialize(JsonParser p, DeserializationContext deserializationContext) throws IOException {
            return DateUtil.parseZonedDateTimeWithFormatter(p.getValueAsString()).withZoneSameInstant(ZoneId.systemDefault()).toLocalTime();
        }
    }

}
