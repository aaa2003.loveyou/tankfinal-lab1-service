package com.tankfinal.lab1service.config.mybatis.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.tankfinal.lab.core.auth.AuthHandler;
import com.tankfinal.lab1service.config.mybatis.config.constant.TableField;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 字段填充處理器
 *
 * @author Tank.Yang
 * @since 2020-06-17 09:16:16
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        if (Objects.isNull(this.getFieldValByName(TableField.CREATE_DATE, metaObject))) {
            this.strictInsertFill(metaObject, TableField.CREATE_DATE, LocalDateTime.class, LocalDateTime.now());
        }
        if (Objects.isNull(this.getFieldValByName(TableField.CREATE_ID, metaObject))) {
            this.strictInsertFill(metaObject, TableField.CREATE_ID, String.class, AuthHandler.getUsername());
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        if (Objects.isNull(this.getFieldValByName(TableField.UPDATE_DATE, metaObject))) {
            this.strictUpdateFill(metaObject, TableField.UPDATE_DATE, LocalDateTime.class, LocalDateTime.now());
        }
        if (Objects.isNull(this.getFieldValByName(TableField.UPDATE_ID, metaObject))) {
            this.strictUpdateFill(metaObject, TableField.UPDATE_ID, String.class, AuthHandler.getUsername());
        }
    }
}