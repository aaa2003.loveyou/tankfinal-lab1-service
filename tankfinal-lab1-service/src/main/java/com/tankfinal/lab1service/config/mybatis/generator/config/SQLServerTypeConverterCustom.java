package com.tankfinal.lab1service.config.mybatis.generator.config;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.ITypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;

public class SQLServerTypeConverterCustom extends SQLServerTypeConverter implements ITypeConvert {

    @Override
    public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
        String t = fieldType.toLowerCase();
        if (t.contains("tinyint") || t.contains("int")) {
            return DbColumnType.INTEGER;
        }
        if (t.contains("decimal") || t.contains("numeric")) {
            return DbColumnType.BIG_DECIMAL;
        }
        return super.processTypeConvert(globalConfig, fieldType);
    }

}
