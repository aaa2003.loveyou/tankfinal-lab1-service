package com.tankfinal.lab1service.config.mybatis.config.constant;

/**
 * Table常用常數類
 *
 * @author Tank.Yang
 * @since 2020-06-17 10:32:24
 */
public interface TableField {
    String CREATE_DATE = "createdate";
    String CREATE_ID = "createid";
    String UPDATE_DATE = "updatedate";
    String UPDATE_ID = "updateid";
}
