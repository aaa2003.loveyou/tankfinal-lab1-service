package com.tankfinal.lab1service.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Web層設定控制器
 *
 * @author Tank.Yang
 * @since 2022-06-08 12:53:54
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 跨域過濾器
     * 攔截器執行順序屬於Servlet層級
     * 若同時配置addCorsMappings與攔截器，收到請求時請求被攔截器優先截走，後續也就不會處理addCorsMappings的內容
     * 而使用corsFilter進行跨域處理，其處理優先順序優先於Servlet。能夠正確處理跨域問題，也能夠同時兼容攔截器
     *
     * @return org.springframework.web.filter.CorsFilter
     * @author Tank.Yang
     * @since 2022-06-08 09:54:42
     */
    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", this.corsConfig());
        return new CorsFilter(source);
    }

    /**
     * 跨域處理配置
     *
     * @return org.springframework.web.cors.CorsConfiguration
     * @author Tank.Yang
     * @since 2022-06-08 10:00:00
     */
    private CorsConfiguration corsConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOriginPattern("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setMaxAge(3600L);
        corsConfiguration.setAllowCredentials(true);
        return corsConfiguration;
    }
}
