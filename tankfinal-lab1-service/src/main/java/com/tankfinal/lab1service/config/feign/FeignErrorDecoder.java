package com.tankfinal.lab1service.config.feign;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.tankfinal.lab.core.constant.Global;
import com.tankfinal.lab.core.exception.customize.InteractionException;
import com.tankfinal.lab.core.exception.model.InteractionExceptionModel;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Feign異常統一解碼器
 *
 * @author Tank.Yang
 * @since 2022-06-27 14:36:37
 */
@Slf4j
@Configuration
public class FeignErrorDecoder extends ErrorDecoder.Default {

    @SneakyThrows
    @Override
    public Exception decode(String methodKey, Response response) {

        // 原始錯誤訊息
        String errorMessage;

        try {
            /*
             * 取得原始錯誤訊息
             * 此處不使用super.decode取得錯誤內容，因為若錯誤訊息過長會在Util.toByteArray方法被替換成xxxx bytes字串
             * 影響後續判斷
             */
            errorMessage = IOUtils.toString(response.body().asInputStream(), StandardCharsets.UTF_8);
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
            return new InteractionException(String.format(FeignErrorDecoderParams.PARSE_ERROR, response.status()));
        }
        if (StringUtils.isBlank(errorMessage)) {
            return new InteractionException(String.format(FeignErrorDecoderParams.EMPTY_ERROR_MESSAGE, response.status()));
        }

        // 開始解析
        Exception e = super.decode(methodKey, response);

        // 僅捕捉fc類的異常
        if (e instanceof FeignException) {

            log.info(FeignErrorDecoderParams.DEFAULT_OUTPUT_MESSAGE, errorMessage);

            if (!JSONUtil.isJson(errorMessage)) {
                return new InteractionException(String.format(FeignErrorDecoderParams.PARSE_ERROR_FAILED, response.status()));
            }
            JSONObject errMsgJsonObj = new JSONObject(errorMessage);

            // 包裝異常訊息
            return new InteractionException(new InteractionExceptionModel() {
                @Override
                public int getCode() {
                    return Integer.parseInt(ObjectUtils.defaultIfNull(errMsgJsonObj.get(FeignErrorDecoderParams.CODE), Global.FAIL).toString());
                }
                @Override
                public String getMessage() {
                    return ObjectUtils.defaultIfNull(errMsgJsonObj.get(FeignErrorDecoderParams.MESSAGE), Strings.EMPTY).toString();
                }
            }, this.parseDetail(errMsgJsonObj.get(FeignErrorDecoderParams.DETAIL)));
        }
        return e;
    }

    /**
     * 解析Detail字串
     *
     * @param object Detail物件
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2022-06-27 14:10:18
     */
    private String parseDetail(Object object) {
        String detail = ObjectUtils.defaultIfNull(object, Strings.EMPTY).toString();
        if (object instanceof String[]) {
            return JSONUtil.toJsonStr(object);
        }
        return detail;
    }

    /**
     * 錯誤訊息參數類
     *
     * @author Tank.Yang
     * @since 2022-06-27 14:18:44
     */
    interface FeignErrorDecoderParams {

        String DEFAULT_OUTPUT_MESSAGE = ">>>>> FeignErrorDecoder: {} <<<<<";

        String PARSE_ERROR = "FC ErrorDecoder發生錯誤，解析異常訊息時發生錯誤，status: %s";
        String EMPTY_ERROR_MESSAGE = "FC ErrorDecoder發生錯誤，異常訊息為空，status: %s";
        String PARSE_ERROR_FAILED = "FC ErrorDecoder發生錯誤，無法解析的格式，status: %s";

        String CODE = "code";
        String MESSAGE = "message";
        String DETAIL = "detail";
    }
}
