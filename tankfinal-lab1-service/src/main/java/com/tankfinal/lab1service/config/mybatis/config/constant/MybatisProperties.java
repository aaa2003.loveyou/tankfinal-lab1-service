package com.tankfinal.lab1service.config.mybatis.config.constant;

/**
 * Mybatis 屬性配置
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:24:09
 */
public interface MybatisProperties {
    /**
     * 配置key
     */
    String CONFIG_KEY = "mybatis-plus.performance";
}
