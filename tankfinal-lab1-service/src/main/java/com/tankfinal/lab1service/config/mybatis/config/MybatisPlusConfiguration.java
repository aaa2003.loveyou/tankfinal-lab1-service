package com.tankfinal.lab1service.config.mybatis.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.tankfinal.lab1service.config.mybatis.config.constant.CustomSqlInjector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MyBatisPlus 配置
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:51:29
 */
@Slf4j
@Configuration
@EnableTransactionManagement
@EnableConfigurationProperties(MybatisConfig.class)
public class MybatisPlusConfiguration {

    /**
     * MyBatisPlus 默認覆蓋的分頁組件
     *
     * @return com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor
     * @author Tank.Yang
     * @since 2020-06-16 13:51:46
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        log.info(">>>>>>>> 注册 MybatisPlus 分頁組件 <<<<<<<<");
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setDbType(DbType.MYSQL);
        return paginationInterceptor;
    }

    @Bean
    public CustomSqlInjector logicSqlInjector() {
        log.info(">>>>>>>> 注册 MybatisPlus 客製化組件 <<<<<<<<");
        return new CustomSqlInjector();
    }

}
