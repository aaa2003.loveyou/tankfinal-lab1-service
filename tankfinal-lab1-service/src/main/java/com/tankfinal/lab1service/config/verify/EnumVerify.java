package com.tankfinal.lab1service.config.verify;

import com.tankfinal.lab.core.exception.customize.SystemException;
import com.tankfinal.lab.core.exception.model.ServiceExceptionModel;
import com.tankfinal.lab.core.exception.model.SystemExceptionModel;
import com.tankfinal.lab1service.annotations.Limit;
import com.tankfinal.lab1service.annotations.Range;
import com.tankfinal.lab1service.constant.Lab1ServiceExceptionConstant;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 列舉工具類
 *
 * @author Tank.Yang
 * @since 2020-09-08 20:18:44
 */
@Slf4j
public final class EnumVerify {

    private EnumVerify() {
    }

    /**
     * 執行初始化檢查
     *
     * @author Tank.Yang
     * @since 2020-09-08 20:18:28
     */
    public static void verify() {
        // 需要檢查的列舉類列表
        List<String> classList = Arrays.asList(
                Lab1ServiceExceptionConstant.class.getName()
        );

        // 遍歷檢查
        classList.forEach(p -> {
            // 獲取當前線程中的類加载器
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            Class<?> aClass;
            try {
                aClass = contextClassLoader.loadClass(p);
            } catch (ClassNotFoundException e) {
                throw new SystemException(p + ".class 不存在,請檢查配置");
            }

            final Set<Integer> codeSet = new HashSet<>();
            AtomicInteger len = new AtomicInteger();

            Optional
                    .ofNullable(aClass)
                    .ifPresent(a -> {
                                Optional
                                        .ofNullable(a.getDeclaredClasses())
                                        .ifPresent(ds -> Arrays.stream(ds)
                                                .forEach(d -> Optional
                                                        .ofNullable(d)
                                                        .filter(Class::isEnum)
                                                        .ifPresent(es -> {
                                                            Enum[] enumConstants = ((Class<Enum>) es).getEnumConstants();
                                                            checkRangeLimit(enumConstants[0].getClass());
                                                            Arrays.stream(enumConstants)
                                                                    .forEach(e ->
                                                                            codeSet.add(getCode(e)));
                                                            len.addAndGet(enumConstants.length);
                                                        })
                                                ));

                                Optional
                                        .of(len.get())
                                        .filter(l -> l == codeSet.size())
                                        .orElseThrow(() -> new SystemException("錯誤列舉類中包含重複code,請檢查代码"));
                            }

                    );

        });
    }


    /**
     * 檢查範圍限制
     *
     * @param tClass 需要檢查的列舉類型
     * @author Tank.Yang
     * @since 2020-09-08 20:20:56
     */
    @SuppressWarnings("unchecked")
    private static void checkRangeLimit(Class<? extends Enum> tClass) {
        Range annotation = tClass.getAnnotation(Range.class);
        // 獲取當前Range limit限制
        Limit limit = annotation.limit();
        // 獲取當前列舉類
        EnumSet enumSet = EnumSet.allOf(tClass);
        // 當前列舉類中列舉總数
        int initSize = enumSet.size();
        // 存放當前列舉類型所有code
        final Set noRepeat = new HashSet(initSize);

        // 列舉code必須是在@Range範圍區間
        enumSet.forEach(e -> {
                    Enum anEnum = Optional
                            .of(e)
                            .filter(Enum.class::isInstance)
                            .map(Enum.class::cast)
                            .get();

                    Integer integer = Optional
                            .of(getCode(anEnum))
                            .filter(p -> p >= limit.getStart() && p <= limit.getEnd())
                            .orElseGet(() -> {
                                log.error("類名：[" + tClass.getName() + "] @Range.limit配置錯誤, 請檢查code是否不在@Range.limit區間");
                                throw new SystemException("類名：[" + tClass.getName() + "] @Range.limit配置錯誤, 請檢查code是否不在@Range.limit區間");
                            });
                    // 將解析出的code 存放在不重複的set副本中
                    noRepeat.add(integer);
                }
        );

        // 檢查code是否重複
        Optional
                .of(initSize)
                .filter(p -> p == noRepeat.size())
                .orElseThrow(() -> new SystemException(tClass.getName() + " code值重複,請檢查代码"));
    }

    /**
     * 獲取配置code
     *
     * @param e 列舉類
     * @return int 取得的Code
     * @author Tank.Yang
     * @since 2020-09-16 17:42:14
     */
    private static int getCode(Enum e) {
        if (e instanceof ServiceExceptionModel) {
            return Optional
                    .of(e)
                    .map(ServiceExceptionModel.class::cast)
                    .get()
                    .getCode();
        } else if (e instanceof SystemExceptionModel) {
            return Optional
                    .of(e)
                    .map(SystemExceptionModel.class::cast)
                    .get()
                    .getCode();
        }
        throw new SystemException(new SystemException(e.getClass().getName() + " 必須實現對應的錯誤類别接口"));
    }
}
