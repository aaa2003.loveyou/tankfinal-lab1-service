package com.tankfinal.lab1service.config.mybatis.config.constant;

/**
 * 要客製化的的SQL方法列舉
 *
 * @author Tank.Yang
 * @since 2020-09-12 16:05:51
 */
public enum CustomSqlMethod {
    // 插入
    INSERT_BATCH("insertBatch", "插入一條數據(選擇字段插入)", "<script>\nINSERT INTO %s %s VALUES %s\n</script>"),
    ;

    private final String method;
    private final String desc;
    private final String sql;

    CustomSqlMethod(String method, String desc, String sql) {
        this.method = method;
        this.desc = desc;
        this.sql = sql;
    }

    public String getMethod() {
        return method;
    }

    public String getDesc() {
        return desc;
    }

    public String getSql() {
        return sql;
    }
}