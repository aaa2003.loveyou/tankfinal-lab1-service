package com.tankfinal.lab1service.config.mybatis.config;

import com.tankfinal.lab1service.config.mybatis.config.constant.MybatisProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Mybatis 配置類
 *
 * @author Tank.Yang
 * @since 2020-06-20 10:09:45
 */
@Getter
@Setter
@ConfigurationProperties(prefix = MybatisProperties.CONFIG_KEY)
public class MybatisConfig {

    /**
     * 最大時間
     */
    private long maxTime;
    /**
     * 是否格式化
     */
    private boolean format;
}