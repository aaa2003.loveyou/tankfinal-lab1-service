package com.tankfinal.lab1service.config.mybatis.generator.config;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.google.common.collect.Lists;
import com.tankfinal.lab.core.exception.customize.ServiceException;
import com.tankfinal.lab.core.util.YmlUtil;
import com.tankfinal.lab1service.config.mybatis.config.AbstractServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 代碼生成器配置類
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:35:17
 */
@Slf4j
public class GeneratorConfiguration {
    /**
     * 配置對象
     */
    private static final GeneratorConfig GENERATOR_CONFIG;
    /**
     * 數據庫配置對象
     */
    private static final GeneratorConfig.Db DB;
    /**
     * 文件配置對象
     */
    private static final GeneratorConfig.File FILE;
    /**
     * 全局配置對象
     */
    private static final GeneratorConfig.Global GLOBAL;
    /**
     * 包配置對象
     */
    private static final GeneratorConfig.Pack PACK;
    /**
     * 项目配置對象
     */
    private static final GeneratorConfig.Project PROJECT;

    // 讀取配置文件,初始化配置對象
    static {
        Map<String, Object> yml = YmlUtil.getYml("/mybatis/generator.yml");
        GENERATOR_CONFIG = JSONObject.toJavaObject(new JSONObject(yml), GeneratorConfig.class);
        DB = GENERATOR_CONFIG.getDb();
        FILE = GENERATOR_CONFIG.getFile();
        GLOBAL = GENERATOR_CONFIG.getGlobal();
        PACK = GENERATOR_CONFIG.getPack();
        PROJECT = GENERATOR_CONFIG.getProject();
    }

    /**
     * 獲取生成文件配置
     *
     * @return com.baomidou.mybatisplus.generator.config.GlobalConfig
     * @author Tank.Yang
     * @since 2020-06-16 13:38:10
     */
    public static GlobalConfig getGlobalConfig() {
        log.info("文件輸出路徑:{}", FILE.getOutPutDir());
        log.info("是否覆蓋本地已生成的文件:{}", FILE.isFileOverRide());
        // 全局配置
        GlobalConfig config = new GlobalConfig();

        // 輸出文件路徑
        config.setOutputDir(Optional
                .ofNullable(FILE.getOutPutDir())
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new ServiceException("outputdir cannot be empty")));

        // 是否覆蓋舊文件
        config.setFileOverride(FILE.isFileOverRide());
        // 作者
        config.setAuthor(PROJECT.getAuthor());
        // XML ResultMap
        config.setBaseResultMap(GLOBAL.isGenBaseResultMap());
        // XML columList
        config.setBaseColumnList(GLOBAL.isGenBaseColumnList());
        // 不需要ActiveRecord特性的請改為false
        config.setActiveRecord(false);
        // XML 二級緩存
        config.setEnableCache(false);
        config.setSwagger2(true);
        // 自定義文艦命名，注意 %s 會自動填充表實體屬性
        config.setControllerName("%sController");
        config.setServiceName("%sService");
        config.setServiceImplName("%sServiceImpl");
        config.setMapperName("%sMapper");
        config.setXmlName("%sDao");
        return config;
    }

    /**
     * 獲取數據源配置
     *
     * @return com.baomidou.mybatisplus.generator.config.DataSourceConfig
     * @author Tank.Yang
     * @since 2020-06-16 13:49:04
     */
    public static DataSourceConfig getDataSourceConfig() {
        DataSourceConfig config = new DataSourceConfig();
        config.setDbType(DbType.SQL_SERVER);
        config.setDriverName(Optional
                .ofNullable(DB.getDriver())
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new ServiceException("db driver cannot be empty")));

        config.setUrl(Optional
                .ofNullable(DB.getUrl())
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new ServiceException("db url cannot be empty")));

        config.setUsername(Optional
                .ofNullable(DB.getUser())
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new ServiceException("db user cannot be empty")));

        config.setPassword(DB.getPassword());

        config.setTypeConvert(new SQLServerTypeConverterCustom());
        return config;
    }

    /**
     * 生成策略配置
     *
     * @param tables 需要生成的表名,可以式多個,參數為動態參數,可以傳入多個
     * @return com.baomidou.mybatisplus.generator.config.StrategyConfig
     * @author Tank.Yang
     * @since 2020-06-16 13:48:17
     */

    public static StrategyConfig getStrategyConfig(String... tables) {
        StrategyConfig config = new StrategyConfig();
        // 统一表前缀
        String tablePrefix = GLOBAL.getTablePrefix();
        if (StringUtils.isNotBlank(tablePrefix)) {
            config.setTablePrefix(tablePrefix);
        }
        // 是否生成實體類常量字段
        config.setEntityColumnConstant(GLOBAL.isGenEntityColumnConstant());
        // 是否按照Lombok模板生成
        config.setEntityLombokModel(GLOBAL.isGenEntityLombokModel());
        // 是否按照Lombok生成Build
        config.setChainModel(GLOBAL.isGenEntityBuilderModel());
        // 表名生成策略
        config.setNaming(NamingStrategy.underline_to_camel);
        // 需要生成的表
        config.setInclude(tables);

        // ---- tankfinal config start----

        // iService實現類改成自定義的類
        config.setSuperServiceImplClass(AbstractServiceImpl.class);
        List<TableFill> tableFillList = Lists.newArrayList();
        tableFillList.add(new TableFill("updateid", FieldFill.UPDATE));
        tableFillList.add(new TableFill("updatedate", FieldFill.UPDATE));
        tableFillList.add(new TableFill("createid", FieldFill.INSERT));
        tableFillList.add(new TableFill("createdate", FieldFill.INSERT));
        config.setTableFillList(tableFillList);

        // ---- tankfinal config end----

        return config;
    }

    /**
     * 包配置
     *
     * @return com.baomidou.mybatisplus.generator.config.PackageConfig
     * @author Tank.Yang
     * @since 2020-06-16 13:48:42
     */

    public static PackageConfig getPackageConfig() {
        // 全路徑包名 = 包名+工程名
        String parent =
                Optional
                        .of(PACK.getName())
                        .filter(StringUtils::isNotBlank)
                        .orElseThrow(() -> new ServiceException("pack name cannot be empty"))
                        + "."
                        + Optional
                        .of(PROJECT.getName())
                        .filter(StringUtils::isNotBlank)
                        .orElseThrow(() -> new ServiceException("project name cannot be empty"));

        log.info("包名:{}", parent);
        PackageConfig config = new PackageConfig();
        config.setParent(parent);
        config.setController(PACK.getController());
        config.setService(PACK.getService());
        config.setServiceImpl(PACK.getServiceImpl());
        config.setMapper(PACK.getMapper());
        config.setEntity(PACK.getEntity());
        config.setXml(PACK.getXml());
        return config;
    }
}
