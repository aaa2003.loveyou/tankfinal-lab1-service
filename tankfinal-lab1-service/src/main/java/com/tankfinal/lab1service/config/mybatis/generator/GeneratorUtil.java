package com.tankfinal.lab1service.config.mybatis.generator;

import com.tankfinal.lab1service.config.mybatis.generator.handler.GeneratorHandler;

/**
 * MyBatis代碼生成工具
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:30:16
 */
public class GeneratorUtil {

    public static void main(String[] args) {
        GeneratorHandler.generator("");
    }

}
