package com.tankfinal.lab1service.config.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Feign攔截器
 *
 * @author Tank.Yang
 * @since 2022-02-17 18:13:34
 */
@Configuration
public class FeignConfig implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        HttpServletRequest request = getHttpServletRequest();
        if (Objects.isNull(request)) {
            return;
        }
        Map<String, String> headers = this.getHeaders(request);
        if (headers.size() > 0) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                // 移除分塊傳輸編碼，避免Feign過程中出現錯誤
                if ("transfer-encoding".equalsIgnoreCase(entry.getKey())) {
                    continue;
                }
                requestTemplate.header(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * 取得HTTP請求
     *
     * @return javax.servlet.http.HttpServletRequest
     * @author Tank.Yang
     * @since 2022-01-28 09:13:51
     */
    private HttpServletRequest getHttpServletRequest() {
        try {
            return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 配置標頭，配置seata全局事務的xid
     *
     * @param request 請求物件
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @author Tank.Yang
     * @since 2022-01-22 17:29:40
     */
    private Map<String, String> getHeaders(HttpServletRequest request) {
        Map<String, String> map = new LinkedHashMap<>();

        if (Objects.isNull(request)) {
            return map;
        }
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }

}