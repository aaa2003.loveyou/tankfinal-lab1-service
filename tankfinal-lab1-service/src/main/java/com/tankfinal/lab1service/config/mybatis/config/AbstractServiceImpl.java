package com.tankfinal.lab1service.config.mybatis.config;

import com.baomidou.mybatisplus.core.enums.SqlMethod;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.baomidou.mybatisplus.core.toolkit.*;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.tankfinal.lab1service.config.mybatis.config.constant.CustomSqlMethod;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.session.SqlSession;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 * 重寫ServiceImpl,當批次插入時不處理AutoGenerateKey
 *
 * @author Tank.Yang
 * @since 2020-09-12 16:06:54
 */
public abstract class AbstractServiceImpl<E extends BaseMapper<T>, T> extends ServiceImpl<E, T> implements IService<T> {
    /**
     * 空的TableId
     */
    private final static String MSG_EMPTY_TABLE_ID = "Error:  Can not execute. Could not find @TableId.";
    /**
     * 空的Collection
     */
    private final static String MSG_EMPTY_ENTITY_LIST = "Error: entityList must not be empty";

    /**
     * 複寫批次插入
     *
     * @param entityList 執行插入的Collection
     * @param batchSize  批次大小
     * @return boolean
     * @author Tank.Yang
     * @since 2020-09-12 16:08:19
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveBatch(Collection<T> entityList, int batchSize) {

        int i = 0;
        String sqlStatement = SqlHelper.table(currentModelClass()).getSqlStatement(CustomSqlMethod.INSERT_BATCH.getMethod());
        try (SqlSession batchSqlSession = sqlSessionBatch()) {
            for (T anEntityList : entityList) {
                batchSqlSession.insert(sqlStatement, anEntityList);
                if (i >= 1 && i % batchSize == 0) {
                    batchSqlSession.flushStatements();
                }
                i++;
            }
            batchSqlSession.flushStatements();
        }
        return Boolean.TRUE;
    }

    /**
     * 複寫批次或更新插入
     *
     * @param entityList 執行插入的Collection
     * @param batchSize  批次大小
     * @return boolean
     * @author Tank.Yang
     * @since 2020-09-12 16:08:19
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean saveOrUpdateBatch(Collection<T> entityList, int batchSize) {
        if (CollectionUtils.isEmpty(entityList)) {
            throw new IllegalArgumentException(MSG_EMPTY_ENTITY_LIST);
        }
        Class<?> cls = currentModelClass();
        TableInfo tableInfo = TableInfoHelper.getTableInfo(cls);
        int i = 0;
        try (SqlSession batchSqlSession = sqlSessionBatch()) {
            for (T anEntityList : entityList) {
                if (null != tableInfo && StringUtils.isNotBlank(tableInfo.getKeyProperty())) {
                    Object idVal = ReflectionKit.getMethodValue(cls, anEntityList, tableInfo.getKeyProperty());
                    if (StringUtils.checkValNull(idVal) || Objects.isNull(getById((Serializable) idVal))) {
                        batchSqlSession.insert(SqlHelper.table(currentModelClass())
                                .getSqlStatement(CustomSqlMethod.INSERT_BATCH.getMethod()), anEntityList);
                    } else {
                        MapperMethod.ParamMap<T> param = new MapperMethod.ParamMap<>();
                        param.put(Constants.ENTITY, anEntityList);
                        batchSqlSession.update(sqlStatement(SqlMethod.UPDATE_BY_ID), param);
                    }
                    // 清除批次插入的Session
                    if (i >= 1 && i % batchSize == 0) {
                        batchSqlSession.flushStatements();
                    }
                    i++;
                } else {
                    throw ExceptionUtils.mpe(MSG_EMPTY_TABLE_ID);
                }
                batchSqlSession.flushStatements();
            }
        }
        return Boolean.TRUE;
    }

}

