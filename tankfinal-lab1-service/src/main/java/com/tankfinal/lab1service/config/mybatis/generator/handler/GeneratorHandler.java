package com.tankfinal.lab1service.config.mybatis.generator.handler;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.tankfinal.lab1service.config.mybatis.generator.config.GeneratorConfiguration;
import lombok.extern.slf4j.Slf4j;


/**
 * MybatisPlus代碼自動生成處理器
 *
 * @author Tank.Yang
 * @since 2020-06-16 13:33:58
 */
@Slf4j
public class GeneratorHandler {

    /**
     * 執行生成代碼
     *
     * @param tables 表名
     * @author Tank.Yang
     * @since 2020-06-16 13:33:44
     */
    public static void generator(String... tables) {
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(GeneratorConfiguration.getGlobalConfig());
        autoGenerator.setDataSource(GeneratorConfiguration.getDataSourceConfig());
        autoGenerator.setStrategy(GeneratorConfiguration.getStrategyConfig(tables));
        autoGenerator.setPackageInfo(GeneratorConfiguration.getPackageConfig());

        log.info("=================== 生成文件 BEGIN ===================");
        for (String table : tables) {
            log.info("表名:{}", table);
        }
        autoGenerator.execute();
        log.info("=================== 生成文件 END ===================");
    }

}
