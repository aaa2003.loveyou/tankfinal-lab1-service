package com.tankfinal.lab1service.runner;

import com.tankfinal.lab.core.constant.Global;
import com.tankfinal.lab1service.runner.init.CodeCheckInit;
import com.tankfinal.lab1service.runner.init.RabbitCheckInit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 容器啟動檢查
 *
 * @author Tank.Yang
 * @since 2020-09-08 20:28:06
 */
@Order(0)
@Component
@Slf4j
@ConditionalOnProperty(prefix = Global.Config.JOB_PREFIX , name = "enable", havingValue = "true", matchIfMissing = true)
@EnableConfigurationProperties(JobRunnerConfig.class)
public class InitRunner implements CommandLineRunner {

    /**
     * 防止重複加載
     */
    private boolean load = false;

    private final CodeCheckInit codeCheckInit;
    private final RabbitCheckInit rabbitCheckInit;

    public InitRunner(CodeCheckInit codeCheckInit, RabbitCheckInit rabbitCheckInit) {
        this.codeCheckInit = codeCheckInit;
        this.rabbitCheckInit = rabbitCheckInit;
    }

    @Override
    public void run(String... args) {
        log.info(">>>>>>>> 初始化Job <<<<<<<<");
        if (load) {
            return;
        }
        load = true;
        // 檢查rabbit伺服器是否存在，若不存在，強制關閉服務
//        rabbitCheckInit.exec();
        // 檢查code配置
        codeCheckInit.exec();
    }
}
