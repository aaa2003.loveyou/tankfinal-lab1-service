package com.tankfinal.lab1service.runner;

import com.tankfinal.lab.core.constant.Global;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 *  JobRunner配置类
 * </p>
 * @author Tank
 * @since 2019-11-21 11:35:52
 */
@Getter
@Setter
@ConfigurationProperties(prefix = Global.Config.JOB_PREFIX)
public class JobRunnerConfig {
    /** 是否啟用 */
    private boolean enable;
}