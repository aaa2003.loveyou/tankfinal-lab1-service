package com.tankfinal.lab1service.runner;

/**
 * 初始化任務接口,必須實現該接口
 *
 * @author Tank.Yang
 * @since 2020-09-08 20:23:52
 */
public interface InitApi {

    /**
     * 執行初始化任務
     *
     * @author Tank.Yang
     * @since 2020-09-08 20:23:35
     */
    void exec();

    /**
     * 重新執行任務
     *
     * @author Tank.Yang
     * @since 2021-10-01 10:46:25
     */
    void reDo();
}
