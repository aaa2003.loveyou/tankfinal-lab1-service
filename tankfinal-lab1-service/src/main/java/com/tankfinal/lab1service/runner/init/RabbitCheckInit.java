package com.tankfinal.lab1service.runner.init;

import com.tankfinal.lab1service.runner.InitApi;
import com.tankfinal.lab1service.runner.verify.RabbitVerify;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Code檢查任務
 *
 * @author Tank.Yang
 * @since 2020-09-08 20:24:04
 */
@Slf4j
@Component
public class RabbitCheckInit implements InitApi {

    /**
     * <p>
     * 校驗列舉
     * 該方法不進行異常處理，如果拋出異常，則整個應用無法啟動，強制驗證
     * </p>
     *
     * @author Tank.Yang
     * @since 2020-09-08 20:23:22
     */
    @Override
    public void exec() {
        log.info(">>>>>>>> 檢查RabbitMQ是否連線，若連線失敗，強制關閉服務 <<<<<<<<");
        RabbitVerify.verify();
    }

    /**
     * 重新執行任務
     *
     * @author Tank.Yang
     * @since 2021-10-01 10:46:25
     */
    @Override
    public void reDo() {

    }
}
