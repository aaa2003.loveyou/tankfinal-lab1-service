package com.tankfinal.lab1service.constant;

import com.tankfinal.lab.core.exception.model.ServiceExceptionModel;
import com.tankfinal.lab1service.annotations.Limit;
import com.tankfinal.lab1service.annotations.Range;

/**
 * 模組錯誤常數列舉類
 *
 * @author Tank.Yang
 * @since 2020-07-09 14:43:24
 */
public interface Lab1ServiceExceptionConstant {
    /**
     * 數據校驗相關
     *
     * @author Tank.Yang
     * @since 2021-08-31 16:58:53
     */
    @Range(limit = Limit.VALID)
    enum DataValidExceptionEnum implements ServiceExceptionModel {
        /**
         * 以下定義 數據校驗相關 列舉
         */
        DATA_VALID_FAILED(96060901, "資料驗證失敗:%s"),
        DATA_IS_UNUSUAL(96060902, "數據異常");

        private final int code;
        private final String message;

        DataValidExceptionEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }

        @Override
        public int getCode() {
            return this.code;
        }

        @Override
        public String getMessage() {
            return this.message;
        }
    }

}
