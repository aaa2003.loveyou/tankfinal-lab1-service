package com.tankfinal.lab1service.filter;

import com.tankfinal.lab.core.auth.AuthHandler;
import com.tankfinal.lab.core.constant.MDCConstant;
import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.util.UUID;

/**
 * MDC 過濾器，自定義log屬性值
 *
 * @author Tank.Yang
 * @since 2022-03-25 16:21:25
 */
@Component
@Order(1)
public class MDCFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        MDC.put(MDCConstant.SESSION_ID, UUID.randomUUID().toString().replace("-", "").substring(0, 12));
        MDC.put(MDCConstant.USER_NAME, AuthHandler.getUsername());
        filterChain.doFilter(servletRequest, servletResponse);
        MDC.clear();
    }
}
