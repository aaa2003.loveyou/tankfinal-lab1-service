package com.tankfinal.lab1service.mybatis.iservice.test;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tankfinal.lab1serviceapi.model.test.PersonalDataDetail;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
public interface PersonalDataDetailService extends IService<PersonalDataDetail> {

}
