package com.tankfinal.lab1service.mybatis.iservice.test.impl;

import com.tankfinal.lab1service.config.mybatis.config.AbstractServiceImpl;
import com.tankfinal.lab1service.mybatis.iservice.test.PersonalDataDetailService;
import com.tankfinal.lab1service.mybatis.mapper.test.PersonalDataDetailMapper;
import com.tankfinal.lab1serviceapi.model.test.PersonalDataDetail;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
@Service
public class PersonalDataDetailServiceImpl extends AbstractServiceImpl<PersonalDataDetailMapper, PersonalDataDetail> implements PersonalDataDetailService {

}
