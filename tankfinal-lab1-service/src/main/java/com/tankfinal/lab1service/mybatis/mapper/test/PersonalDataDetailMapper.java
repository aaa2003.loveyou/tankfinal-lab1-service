package com.tankfinal.lab1service.mybatis.mapper.test;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tankfinal.lab1serviceapi.model.test.PersonalDataDetail;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
public interface PersonalDataDetailMapper extends BaseMapper<PersonalDataDetail> {

}
