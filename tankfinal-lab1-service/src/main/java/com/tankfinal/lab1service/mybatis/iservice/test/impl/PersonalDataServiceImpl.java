package com.tankfinal.lab1service.mybatis.iservice.test.impl;

import com.tankfinal.lab1service.config.mybatis.config.AbstractServiceImpl;
import com.tankfinal.lab1service.mybatis.iservice.test.PersonalDataService;
import com.tankfinal.lab1service.mybatis.mapper.test.PersonalDataMapper;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
@Service
public class PersonalDataServiceImpl extends AbstractServiceImpl<PersonalDataMapper, PersonalData> implements PersonalDataService {

}
