package com.tankfinal.lab1service.mybatis.iservice.test;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
public interface PersonalDataService extends IService<PersonalData> {


}
