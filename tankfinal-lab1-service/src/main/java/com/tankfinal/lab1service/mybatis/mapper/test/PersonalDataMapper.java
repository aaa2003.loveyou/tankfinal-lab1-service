package com.tankfinal.lab1service.mybatis.mapper.test;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tankfinal.lab1serviceapi.components.IPageCustom;
import com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
public interface PersonalDataMapper extends BaseMapper<PersonalData> {

    /**
     * 取得人員資料
     *
     * @param page 分頁參數
     * @param name 姓名
     * @return com.tankfinal.apsserviceapi.components.IPageCustom<com.tankfinal.apsserviceapi.dto.test.ApsPersonalDataDTO>
     * @author Tank.Yang
     * @since 2020-09-08 14:03:04
     */
    IPageCustom<PersonalDataDTO> getPersonByJoin(IPageCustom<PersonalDataDTO> page, @Param("name") String name);



}
