package com.tankfinal.lab1service.test;

import java.util.concurrent.Semaphore;

public class SemaphoreAcquireExample {
    private Semaphore semaphore = new Semaphore(5);

    public void performTask() {
        try {
            semaphore.acquire(); // 獲取許可證
            Thread.sleep(1000);
            System.out.println(Thread.currentThread().getName() + " acquired the permit");
            // 執行任務
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release(); // 釋放許可證
            System.out.println(Thread.currentThread().getName() + " released the permit");
        }
    }

    public static void main(String[] args) {
        SemaphoreAcquireExample example = new SemaphoreAcquireExample();

        // 啟動多個線程進行任務
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                example.performTask();
            }).start();
        }
    }
}
