package com.tankfinal.lab1service.test;

import java.util.concurrent.Semaphore;

public class SemaphoreLockExample {
    private Semaphore semaphore = new Semaphore(1); // 設置許可證數量為1
    private int count = 0;

    public void increment() throws InterruptedException {
        semaphore.acquire(); // 獲取許可證
        try {
            count++;
        } finally {
            semaphore.release(); // 釋放許可證
        }
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) {
        SemaphoreLockExample example = new SemaphoreLockExample();

        // 啟動多個線程進行增量操作
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10000; j++) {
                    try {
                        example.increment();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        // 等待所有線程完成
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 輸出最後的計數值
        System.out.println("Final Count: " + example.getCount());
    }
}