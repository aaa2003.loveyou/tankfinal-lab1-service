package com.tankfinal.lab1service.test;

public class VolatileExample implements Runnable {
    private volatile boolean flag = false;

    public void setFlag() {
        try {
            Thread.sleep(2000); // 模擬某些耗時操作
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        flag = true;
    }

    public void printMessage() {
        while (!flag) {
            // 等待 flag 變為 true
            System.out.println("Waiting for the flag to be true...");
        }
        System.out.println("Flag is now true. Do something.");
    }

    @Override
    public void run() {
        if (Thread.currentThread().getName().equals("SetFlagThread")) {
            setFlag();
        } else if (Thread.currentThread().getName().equals("PrintMessageThread")) {
            printMessage();
        }
    }

    public static void main(String[] args) {
        VolatileExample example = new VolatileExample();

        // 啟動一個執行緒設置 flag 為 true
        new Thread(example, "SetFlagThread").start();

        // 啟動另一個執行緒等待 flag 變成 true
        new Thread(example, "PrintMessageThread").start();
    }
}
