package com.tankfinal.lab1service.test;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockExample {
    private Lock lock = new ReentrantLock();
    private int count = 0;

    public void increment() {
        lock.lock(); // 獲取鎖
        try {
            count++;
        } finally {
            lock.unlock(); // 釋放鎖
        }
    }

    public int getCount() {
        lock.lock(); // 獲取鎖
        try {
            return count;
        } finally {
            lock.unlock(); // 釋放鎖
        }
    }

    public static void main(String[] args) {
        ReentrantLockExample example = new ReentrantLockExample();

        // 啟動多個線程進行增量操作
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10000; j++) {
                    example.increment();
                }
            }).start();
        }

        // 等待所有線程完成
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 輸出最後的計數值
        System.out.println("Final Count: " + example.getCount());
    }
}
