package com.tankfinal.lab1service.service.test;

import com.tankfinal.lab1serviceapi.components.IPageCustom;
import com.tankfinal.lab1serviceapi.components.PaginationVO;
import com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;

/**
 * 測試類
 *
 * @author Tank.Yang
 * @since 2020-06-19 14:47:10
 */
public interface TestService {

    /**
     * 基礎數據庫讀取範例
     *
     * @param paginationVO 分頁參數
     * @param name         姓名
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:48:31
     */
    IPageCustom<PersonalData> getPersonalByName(PaginationVO paginationVO, String name);

    /**
     * 數據庫Join範例
     *
     * @param paginationVO 分頁參數
     * @param name         姓名
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:49:54
     */
    IPageCustom<PersonalDataDTO> getPersonalByNameJoined(PaginationVO paginationVO, String name);

    /**
     * 基礎數據庫新增範例
     *
     * @param personalData 範例Entity
     * @author Tank.Yang
     * @since 2020-09-08 17:49:02
     */
    void addPersonal(PersonalData personalData);

    /**
     * 基礎數據庫更新範例
     *
     * @param name 姓名
     * @param age  年齡
     * @author Tank.Yang
     * @since 2020-09-08 17:49:04
     */
    void updatePersonal(String name, Integer age);

    /**
     * 基礎數據庫刪除範例
     *
     * @param id 鍵值
     * @author Tank.Yang
     * @since 2020-09-08 17:49:02
     */
    void deletePersonal(Integer id);

    /**
     * 可預期例外狀況範例
     *
     * @author Tank.Yang
     * @since 2020-09-08 11:07:32
     */
    void exceptedThrow();

    /**
     * 非預期例外狀況範例
     *
     * @author Tank.Yang
     * @since 2020-09-08 11:07:32
     */
    void unexpectedThrow();

    /**
     *
     */
    String testAny();

}
