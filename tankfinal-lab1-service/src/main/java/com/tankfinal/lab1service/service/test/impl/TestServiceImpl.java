package com.tankfinal.lab1service.service.test.impl;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.tankfinal.lab.core.result.Result;
import com.tankfinal.lab1service.feign.FeignServiceA;
import com.tankfinal.lab1service.mybatis.iservice.test.PersonalDataService;
import com.tankfinal.lab1service.mybatis.mapper.test.PersonalDataMapper;
import com.tankfinal.lab1service.service.base.BaseService;
import com.tankfinal.lab1service.service.test.TestService;
import com.tankfinal.lab1serviceapi.components.IPageCustom;
import com.tankfinal.lab1serviceapi.components.IPageCustomImpl;
import com.tankfinal.lab1serviceapi.components.PaginationVO;
import com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 測試Service
 *
 * @author Tank.Yang
 * @since 2020-06-17 16:11:35
 */
@Service
@Slf4j
public class TestServiceImpl extends BaseService implements TestService {
    private final PersonalDataService personalDataService;
    private final PersonalDataMapper personalDataMapper;
    private final FeignServiceA feignServiceA;
    @Autowired(required = false)
    public TestServiceImpl(PersonalDataMapper personalDataMapper, PersonalDataService personalDataService, FeignServiceA feignServiceA) {
        this.personalDataMapper = personalDataMapper;
        this.personalDataService = personalDataService;
        this.feignServiceA = feignServiceA;
    }


    /**
     * 基礎數據庫讀取範例
     *
     * @param paginationVO 分頁參數
     * @param name         姓名
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:48:31
     */
    @Override
    public IPageCustom<PersonalData> getPersonalByName(PaginationVO paginationVO, String name) {
        LambdaQueryWrapper<PersonalData> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PersonalData::getName, name);
        return personalDataService.page(new IPageCustomImpl<>(paginationVO), queryWrapper);
    }

    /**
     * 基礎數據庫新增範例
     *
     * @param personalData 範例Entity
     * @author Tank.Yang
     * @since 2020-09-07 13:48:31
     */
    @Override
    public void addPersonal(PersonalData personalData) {
//        log.info("JSONUtil.toJsonStr(PersonalData)=" + JSONUtil.toJsonStr(personalData));
//        Integer resultId = taskProcessMainMapper.getUspPreTPMIDByTPMID(32);
//        log.info(resultId.toString());
        personalDataService.save(personalData);
//        List<String> cncMacModelNoList = machineServiceAPI.getCncMacModelNoList();
    }

    /**
     * 基礎數據庫更新範例
     *
     * @param name 姓名
     * @param age  年齡
     * @author Tank.Yang
     * @since 2020-09-07 13:48:31
     */
    @Override
    public void updatePersonal(String name, Integer age) {
        // 基礎寫法
        LambdaUpdateWrapper<PersonalData> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(PersonalData::getName, name)
                .set(PersonalData::getAge, age);
        personalDataService.update(lambdaUpdateWrapper);

        // chain寫法
        LambdaUpdateChainWrapper<PersonalData> lambdaUpdateChainWrapper = new LambdaUpdateChainWrapper<>(personalDataMapper);
        lambdaUpdateChainWrapper
                .eq(PersonalData::getName, name)
                .set(PersonalData::getAge, age)
                .update();

        // 使用字段填充處理器
        PersonalData personalData = new PersonalData();
        personalData.setId(9);
        personalData.setAge(age);
        personalDataService.updateById(personalData);
    }

    /**
     * 基礎數據庫刪除範例
     *
     * @param id 鍵值
     * @author Tank.Yang
     * @since 2020-09-08 17:49:02
     */
    @Override
    public void deletePersonal(Integer id) {
        // 基礎寫法
        LambdaUpdateWrapper<PersonalData> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper
                .eq(PersonalData::getId, id);
        personalDataService.update(lambdaUpdateWrapper);

//        // 刪除實體資料
//        apsPersonalDataService.removeById(id);
//
//        // chain寫法
//        LambdaUpdateChainWrapper<ApsPersonalData> lambdaUpdateChainWrapper = new LambdaUpdateChainWrapper<>(apsPersonalDataMapper);
//        lambdaUpdateChainWrapper
//                .eq(ApsPersonalData::getId, id)
//                .set(ApsPersonalData::getValidFlag, VALID_FLAG.INVALID.intValue)
//                .update();
//
//        // 使用字段填充處理器
//        ApsPersonalData apsPersonalData = new ApsPersonalData();
//        apsPersonalData.setId(id);
//        apsPersonalData.setValidFlag(VALID_FLAG.INVALID.intValue);
//        log.info(String.valueOf(apsPersonalDataService.updateById(apsPersonalData)));
    }

    /**
     * 數據庫Join範例
     *
     * @param paginationVO 分頁參數
     * @param name 姓名
     * @return com.tankfinal.processserviceapi.components.IPageCustom<com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO>
     * @author Tank.Yang
     * @since 2022-04-08 10:05:22
     */
    @Override
    public IPageCustom<PersonalDataDTO> getPersonalByNameJoined(PaginationVO paginationVO, String name) {
        // 自定義SQL Join分頁範例
        return personalDataMapper.getPersonByJoin(new IPageCustomImpl<>(paginationVO), name);
    }

    /**
     * 可預期例外狀況範例
     *
     * @author Tank.Yang
     * @since 2020-09-08 11:05:32
     */
    @Override
    public void exceptedThrow() {
        // 指定錯誤訊息及參數，%s可無限延伸
    }

    /**
     * 非預期例外狀況範例
     *
     * @author Tank.Yang
     * @since 2020-09-08 11:05:32
     */
    @Override
    public void unexpectedThrow() {
        String str1 = null;
//        try {
        log.info(String.valueOf(Integer.parseInt(str1)));
//        }catch (Exception e){
//            log.error(e.getMessage(), e);
//            // TODO 例外處理, 或不catch, 由ExceptionHandler統一處理
//        }
    }

    @Override
    public String testAny() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        Map<String, String> headers = Collections.list(request.getHeaderNames())
                .stream()
                .collect(
                        java.util.stream.Collectors.toMap(
                                name -> name,
                                request::getHeader
                        )
                );

        headers.forEach((name, value) -> log.info(name + ": " + value));
        String result = feignServiceA.testAny();
        log.info("test result:{}", result);
        return result;
    }

}
