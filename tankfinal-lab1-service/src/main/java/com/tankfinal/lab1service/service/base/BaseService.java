package com.tankfinal.lab1service.service.base;

import com.tankfinal.lab.core.redis.kit.RedisKit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * Service底層類
 *
 * @author Tank.Yang
 * @since 2020-06-19 10:08:18
 */
@Configuration
@Slf4j
public class BaseService {
    /**
     * 分布式中間件
     */
    @Autowired
    protected RedisKit<Object, Object> redisKit;
    @Autowired
    protected RabbitTemplate rabbitTemplate;

}
