package com.tankfinal.lab1service.rabbit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * RabbitMQ消費端
 *
 * @author Tank.Yang
 * @since 2022-01-05 16:38:41
 */
@Slf4j
@Component
public class RabbitConsumer {

}
