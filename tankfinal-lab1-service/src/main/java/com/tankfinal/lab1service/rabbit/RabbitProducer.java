package com.tankfinal.lab1service.rabbit;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ生產端
 *
 * @author Tank.Yang
 * @since 2022-01-05 17:04:40
 */
@Service
@Slf4j
public class RabbitProducer {

}
