package com.tankfinal.lab1service.annotations;

/**
 * Range 注解 限制參數配置
 *
 * @author Tank.Yang
 * @since 2020-09-08 20:07:27
 */
public enum Limit {

    /**
     * limit 限制
     */
    VALID(96060901, 96061000)
    ;
    /**
     * 範圍起止
     */
    private final int start;
    /**
     * 範圍结束
     */
    private final int end;

    Limit(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
}
