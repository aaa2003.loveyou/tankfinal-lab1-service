package com.tankfinal.lab1service.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 範圍列舉,該列舉可注解在類上,在編譯時檢查類屬性是否包含在該範圍内
 *
 * @author Tank.Yang
 * @since 2020-09-08 20:22:16
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Range {

    /**
     * 範圍列舉配置
     *
     * @return com.tankfinal.apsservice.annotations.range.Limit
     * @author Tank.Yang
     * @since 2020-09-08 20:22:19
     */
    Limit limit();
}
