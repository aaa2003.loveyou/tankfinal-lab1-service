package com.tankfinal.lab1service.feign;

import com.tankfinal.lab.core.result.Result;
import com.tankfinal.lab1servicerapi.components.PaginationVO;
import com.tankfinal.lab1servicerapi.model.test.PersonalData;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "lab1-service-r", fallbackFactory = FeignServiceFallbackFactory.class)
public interface FeignServiceA {

    @GetMapping("/api/apiTest/testAny")
    String testAny();
}
