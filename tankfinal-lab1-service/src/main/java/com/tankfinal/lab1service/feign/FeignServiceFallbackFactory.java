package com.tankfinal.lab1service.feign;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FeignServiceFallbackFactory implements FallbackFactory<FeignServiceA> {

    @Override
    public FeignServiceA create(Throwable cause) {
        return new FeignServiceA() {
            @Override
            public String testAny() {
                log.info("fall back");
                return null;
            }
        };
    }
}
