package com.tankfinal.lab1service.feign;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.tankfinal.lab1servicerapi.api.test.TestAPI;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

//@Component
//@Slf4j
public class FeignServiceAImpl {
//    @Override
//    public String testAny() {
//        log.error("Hystrix is coming");
//        return null;
//    }

//    @Autowired
//    TestAPI testAPI;
//
//    @HystrixCommand(commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "90000")}, fallbackMethod = "testFeignFallBack")
//    public void testFeign() {
//        // 在调用 Feign 之前的逻辑
//        log.info("Before Feign Request");
//
//        // 实际的 Feign 调用
//        log.info("Feign: 呼叫測試接口:result{}", testAPI.unexpectedThrow());
//    }
//
//    public void testFeignFallBack() {
//        log.info("Feign: 失敗");
//    }
}