package com.tankfinal.lab1service.monitor;


import com.tankfinal.lab.core.constant.Global;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * API效能監控配置類
 *
 * @author Tank.Yang
 * @since 2022-06-10 18:14:11
 */
@Getter
@Setter
@ConfigurationProperties(prefix = Global.Config.API_PERFORMANCE_MONITOR_PREFIX)
public class APIPerformanceMonitorConfig {
    /** 是否啟用 */
    private boolean enable;
}
