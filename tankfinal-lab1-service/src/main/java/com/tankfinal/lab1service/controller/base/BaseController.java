package com.tankfinal.lab1service.controller.base;

import com.tankfinal.lab.core.constant.Global;
import com.tankfinal.lab.core.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;

/**
 * 基礎Controller，共用方法都在這
 *
 * @author Tank.Yang
 * @since 2020-07-16 14:36:49
 */
@Slf4j
public class BaseController {
    /**
     * 成功訊息
     */
    protected static final Result<Void> SUCCESS = Result.<Void>builder().code(Global.SUCCESS).message(Global.SUCCESS_MSG).build();
    /**
     * 成功訊息帶資料
     */
    protected static final Result.ResultBuilder SUCCESS_DATA = Result.<T>builder().code(Global.SUCCESS).message(Global.SUCCESS_MSG);
    /**
     * 失敗訊息
     */
    protected static final Result<Void> FAILED = Result.<Void>builder().code(Global.FAIL).message(Global.SYSTEM_ERROR).build();
    /**
     * 失敗訊息
     */
    protected static final Result.ResultBuilder FAILED_MSG = Result.<Void>builder().code(Global.FAIL);
}
