package com.tankfinal.lab1service.controller.test;

import cn.hutool.json.JSONUtil;
import com.google.common.collect.Maps;
import com.tankfinal.lab.core.result.Result;
import com.tankfinal.lab1service.controller.base.BaseController;
import com.tankfinal.lab1service.rabbit.RabbitProducer;
import com.tankfinal.lab1service.service.test.TestService;
import com.tankfinal.lab1serviceapi.api.test.TestAPI;
import com.tankfinal.lab1serviceapi.components.DataResultPager;
import com.tankfinal.lab1serviceapi.components.PaginationVO;
import com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

/**
 * 主方法，範例寫法都在這裡
 *
 * @author Tank.Yang
 * @since 2020-06-15 15:33:55
 */
@RestController
@Slf4j
@SuppressWarnings("unchecked")
public class TestController extends BaseController implements TestAPI {

    @Autowired
    RabbitTemplate rabbitTemplate;
    @Autowired
    TestService testService;
    @Autowired
    RabbitProducer rabbitProducer;

    /**
     * 各種測試
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-08-20 11:22:39
     */
    @Override
    public String testAny(String fruit) {
        return testService.testAny();
    }

//    public static void main(String[] args) {
//        Map<Integer, String> map = Maps.newHashMap();
//        map.put(1, "guid");
//        map.put(2, "guid");
//        PersonalData personalData = new PersonalData();
//        personalData.detail = map;
//
//        log.info("personalData:{}", JSONUtil.toJsonStr(personalData));
//    }
    /**
     * 基礎數據庫讀取範例
     *
     * @param paginationVO 分頁參數
     * @param name         姓名
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:48:31
     */
    @Override
    public Result<PersonalData> getPersonalByName(PaginationVO paginationVO, String name) {
        return SUCCESS_DATA.data(testService.getPersonalByName(paginationVO, name)).build();
    }

    /**
     * 數據庫Join範例
     *
     * @param paginationVO 分頁參數
     * @param name         姓名
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:49:54
     */
    @Override
    public DataResultPager<PersonalDataDTO> getPersonalByNameJoined(PaginationVO paginationVO, String name) {
        return new DataResultPager<>(testService.getPersonalByNameJoined(paginationVO, name));
    }

    /**
     * 可預期例外狀況範例
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:49:54
     */
    @Override
    public Result<Void> exceptedThrow() {
        testService.exceptedThrow();
        return SUCCESS;
    }

    /**
     * 可預期例外狀況範例
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:49:54
     */
    @Override
    public Result<Void> unexpectedThrow() {
        testService.unexpectedThrow();
        return SUCCESS;
    }

    /**
     * 基礎數據庫新增範例
     *
     * @param personalData 範例Entity
     * @author Tank.Yang
     * @since 2020-09-08 17:49:02
     */
    @Override
    public Result<Void> addPersonal(@RequestBody PersonalData personalData) {
        testService.addPersonal(personalData);
        return SUCCESS;
    }

    /**
     * 基礎數據庫更新範例
     *
     * @param name 姓名
     * @param age  年齡
     * @author Tank.Yang
     * @since 2020-09-08 17:49:04
     */
    @Override
    public Result<Void> updatePersonal(String name, Integer age) {
        testService.updatePersonal(name, age);
        return SUCCESS;
    }

    /**
     * 基礎數據庫刪除範例
     *
     * @param id 鍵值
     * @author Tank.Yang
     * @since 2020-09-08 17:49:02
     */
    @Override
    public Result<Void> deletePersonal(Integer id) {
        testService.deletePersonal(id);
        return SUCCESS;
    }

    /**
     * MQ推送範例
     *
     * @param msg 欲推送的消息
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-10-12 11:24:02
     */
    @Override
    public Result<Void> testRabbitPush(String msg) {
        log.info("Rabbit Push:{}", msg);
//        rabbitTemplate.convertSendAndReceive(RABBIT_EXCHANGE_NAMES.EXCHANGE_APS_TERMINATE.name, RABBIT_QUEUE_NAMES.QUEUE_APS_PROCESS_CONTROL.name, msg);
        return SUCCESS;
    }

    /**
     * 測試觸發RM RabbitMQ
     *
     * @param taskSectionMainIdSet 主工段ID
     * @return com.tankfinal.lab.core.result.Result<java.lang.Void>
     * @author Vic
     * @since 2022-03-03 14:39:41
     */
    @Override
    public Result<Void> testTriggerRmTask(Set<Integer> taskSectionMainIdSet) {
        return SUCCESS;
    }

}
