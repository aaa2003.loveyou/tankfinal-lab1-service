package com.tankfinal.lab1service.controller.test;

import com.tankfinal.lab.core.result.Result;
import com.tankfinal.lab1service.controller.base.BaseController;
import com.tankfinal.lab1service.rabbit.RabbitProducer;
import com.tankfinal.lab1service.service.test.TestService;
import com.tankfinal.lab1serviceapi.api.test.Test2API;
import com.tankfinal.lab1serviceapi.api.test.TestAPI;
import com.tankfinal.lab1serviceapi.components.DataResultPager;
import com.tankfinal.lab1serviceapi.components.PaginationVO;
import com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * 主方法，範例寫法都在這裡
 *
 * @author Tank.Yang
 * @since 2020-06-15 15:33:55
 */
@RestController
@Slf4j
@SuppressWarnings("unchecked")
public class Test2Controller extends BaseController implements Test2API {

    @Autowired
    TestService testService;
    /**
     * 各種測試
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-08-20 11:22:39
     */
    @Override
    public String testAny2(String fruit) {
        return testService.testAny();
    }
//    public static void main(String[] args) {
//        Map<Integer, String> map = Maps.newHashMap();
//        map.put(1, "guid");
//        map.put(2, "guid");
//        PersonalData personalData = new PersonalData();
//        personalData.detail = map;
//
//        log.info("personalData:{}", JSONUtil.toJsonStr(personalData));
//    }

}
