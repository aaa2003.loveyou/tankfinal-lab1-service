package com.tankfinal.lab1service.aspect;

import cn.hutool.json.JSONUtil;
import com.tankfinal.lab.core.api.ApiPerformanceDTO;
import com.tankfinal.lab.core.constant.Global;
import com.tankfinal.lab.core.constant.Request;
import com.tankfinal.lab1service.monitor.APIPerformanceMonitorConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Optional;

/**
 * <p> 請求切面處理 </p>
 *
 * @author Tank.Yang
 * @since 2020-05-29 14:15:20
 */
@Component
@EnableConfigurationProperties(APIPerformanceMonitorConfig.class)
@ConditionalOnProperty(prefix = Global.Config.API_PERFORMANCE_MONITOR_PREFIX, name = "enable", havingValue = "true", matchIfMissing = true)
@Aspect
@Slf4j
public class RequestAspect {

    @Pointcut("execution(* com.cimforce.materialserviceapi.api..*(..)) || execution(* com.cimforce.materialservice.domain..*Controller.*(..))")
    public void requestAspectPointCut() {
    }

    /**
     * <p> 打印請求相關資訊 </p>
     *
     * @param point ProceedingJoinPoint
     * @return java.lang.Object
     * @throws Throwable 拋出類
     * @author Tank.Yang
     * @since 2020-05-29 16:01:08
     */
    @Around("requestAspectPointCut()")
    public Object loggerAround(ProceedingJoinPoint point) throws Throwable {
        // 起始時間
        long startTime = System.currentTimeMillis();
        // 取得request
        Optional<ServletRequestAttributes> servletRequestAttributesOptional = Optional
                .ofNullable(RequestContextHolder.getRequestAttributes())
                .filter(ServletRequestAttributes.class::isInstance)
                .map(ServletRequestAttributes.class::cast);
        /*
         * 具有HttpServletRequest屬性的請求一定是 from HTTP，若從其他地方呼叫的接口數據將不具有該屬性。
         * 不解析請求內容
         * */
        if (!servletRequestAttributesOptional.isPresent()) {
            log.info(">>>>> [非HTTP] 請求完成, 耗時:{} ms <<<<<", System.currentTimeMillis() - startTime);
            return point.proceed();
        }

        HttpServletRequest request = servletRequestAttributesOptional.get().getRequest();
        // 是否是文件上傳
        boolean isFileUpload = Optional
                .ofNullable(request.getContentType())
                .filter(Request.Head.MULTIPART::startsWith)
                .isPresent();

        String args = "無";

        // 不為文件上傳才解析參數
        if (!isFileUpload) {
            Map<String, String[]> parameterMap = request.getParameterMap();
            if (MapUtils.isNotEmpty(parameterMap)) {
                args = JSONUtil.toJsonStr(parameterMap);
            }
        } else {
            args = "文件上傳";
        }
        Object obj = point.proceed();

        // 起始時間
        long endTime = System.currentTimeMillis();

        log.info(">>>>> [HTTP] 請求完成 URI:{}, 參數:{}, 耗時:{} ms <<<<<", request.getRequestURI(), args, endTime - startTime);

        log.trace(JSONUtil.toJsonStr(ApiPerformanceDTO.builder()
                .timeS(startTime)
                .timeE(endTime)
                .duration(endTime - startTime)
                .timeUnit("MILLISECOND")
                .url(request.getRequestURI())
                .build()));
        return obj;
    }

}
