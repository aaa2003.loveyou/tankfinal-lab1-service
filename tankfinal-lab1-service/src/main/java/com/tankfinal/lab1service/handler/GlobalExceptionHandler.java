package com.tankfinal.lab1service.handler;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.json.JSONUtil;
import com.tankfinal.lab.core.constant.Global;
import com.tankfinal.lab.core.exception.customize.AbstractException;
import com.tankfinal.lab.core.exception.customize.InteractionException;
import com.tankfinal.lab.core.exception.customize.ServiceException;
import com.tankfinal.lab.core.exception.customize.SystemException;
import com.tankfinal.lab.core.result.Result;
import com.tankfinal.lab.core.util.YmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * 全局ErrorHandler
 *
 * @author Tank.Yang
 * @since 2020-12-16 16:03:46
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 全域錯誤統一訊息格式
     */
    private static final String EXCEPTION_MSG = "handler[{}]; source:[{}]; msg:[{}]; url:[{}]";


    /**
     * 新架構Result + 多國語系
     * 若不具多國語系檔，則直接輸出錯誤內容
     *
     * 前端錯誤訊息Dialog內容：
     *
     * |-----------------Dialog-----------------|
     * |-----------  Result.message  -----------| > title
     * |----------------------------------------|
     * |                                        |
     * |             Result.detail              | > content
     * |                                        |
     * |----------------------------------------|
     *
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @param e   Exception
     * @return com.cimforce.commutil.result.Result<?>
     * @author Tank.Yang
     * @since 2020-06-17 11:13:42
     */
    @ExceptionHandler(value = AbstractException.class)
    public Result<?> abstractExceptionHandler(HttpServletRequest req, HttpServletResponse res, Exception e) {
        int code = Global.FAIL;
        String[] data = null;
        String message = null;
        String detail = null;

        if (e instanceof ServiceException) {
            // 業務邏輯異常
            ServiceException se = (ServiceException) e;
            code = se.getCode();
            data = se.getData();
            if (this.hasMessageFile()) {
                message = this.getMessageFileContent(req, code, Global.MultiLocale.TITLE);
                String i18nMsg = this.getMessageFileContent(req, code, Global.MultiLocale.MESSAGE);
                int sLen = StringUtils.countMatches(i18nMsg, "%s");
                // 若無data則把i18nMessage的%s全替換掉; 若有data但數量小於%s的數量，則補足差異的大小。避免format時出錯
                if (ArrayUtils.isEmpty(data)) {
                    i18nMsg = i18nMsg.replace("%s", "");
                } else if (data.length < sLen) {
                    for (int i = 0; i < sLen - data.length; i++) {
                        data = ArrayUtil.append(data, "");
                    }
                }
                detail = String.format(i18nMsg, data);
            } else {
                message = se.getMessage();
                detail = ArrayUtils.isNotEmpty(se.getData()) ? JSONUtil.toJsonStr(se.getData()) : "";
            }
        } else if (e instanceof SystemException) {
            // 系統異常
            SystemException se = (SystemException) e;
            code = se.getCode();
            data = se.getData();
            message = se.getMessage();
            detail = ArrayUtils.isNotEmpty(se.getData()) ? JSONUtil.toJsonStr(se.getData()) : "";
        } else if (e instanceof InteractionException) {
            // 系統交互異常
            InteractionException ie = (InteractionException) e;
            code = ie.getCode();
            data = ie.getData();
            message = ie.getMessage();
            detail = ArrayUtils.isNotEmpty(ie.getData()) ? JSONUtil.toJsonStr(ie.getData()) : "";
        }
        log.error(EXCEPTION_MSG, "AbstractException", e.getClass().getName(), e.getMessage(), req.getRequestURL().toString(), e);
        res.setStatus(Global.MultiLocale.ERROR_CODE);
        // 錯誤訊息dialog > title:message, 內容:detail
        return Result.builder()
                .date(new Date())
                .code(code)
                .url(req.getRequestURL().toString())
                .message(message)
                .detail(detail)
                .data(data)
                .build();
    }

    /**
     * Runtime層級錯誤
     *
     * 前端錯誤訊息Dialog內容
     *
     * |-----------------Dialog-----------------|
     * |-----------  Result.message  -----------| > title
     * |----------------------------------------|
     * |                                        |
     * |             Result.detail              | > content
     * |                                        |
     * |----------------------------------------|
     *
     * @param req HttpServletRequest
     * @param res HttpServletResponse
     * @param e   Exception
     * @return com.cimforce.commutil.result.Result<?>
     * @author Tank.Yang
     * @since 2020-06-17 11:13:42
    */
    @ExceptionHandler(value = Throwable.class)
    public Result<?> runtimeExceptionHandler(HttpServletRequest req, HttpServletResponse res, Exception e) {
        log.error(EXCEPTION_MSG, "Throwable", e.getClass().getName(), e.getMessage(), req.getRequestURL().toString(), e);
        res.setStatus(Global.MultiLocale.ERROR_CODE);
        // 錯誤訊息dialog > title:message, 內容:detail
        return Result.builder()
                .date(new Date())
                .code(Global.FAIL)
                .url(req.getRequestURL().toString())
                .message(Global.Error.UNKNOWN_ERROR)
                .detail(e.getMessage())
                .build();
    }

    /**
     * 取得指定語言的錯誤訊息
     *
     * @param req  HttpServletRequest
     * @param code 錯誤碼
     * @param type 要讀取的內容(TITLE/MESSAGE)
     * @return java.lang.String
     * @author Tank.Yang
     * @since 2020-09-09 18:20:07
     */
    @SuppressWarnings("unchecked")
    private String getMessageFileContent(HttpServletRequest req, Integer code, String type) {
        // 取得指定header語言資訊, 預設CN
        String language = StringUtils.defaultIfBlank(req.getHeader(Global.MultiLocale.LOCALE), Global.MultiLocale.Language.CN);
        // 取得Yml內容
        Map<String, Object> yml = YmlUtil.getYml(Global.MultiLocale.MESSAGE_PATH);
        // 取得分類為error的語系資料
        Map<String, Object> errorCodeMap = (Map<String, Object>) yml.get(Global.MultiLocale.ERROR);
        // 非空判斷
        if (Objects.isNull(errorCodeMap.get(code))) {
            return Global.Error.UNKNOWN_ERROR_CODE;
        }
        Map<Integer, Object> codeMap = (Map<Integer, Object>) errorCodeMap.get(code);
        // 取得對應的語系
        if (Objects.isNull(codeMap.get(language))) {
            return Global.Error.UNKNOWN_LOCALE;
        }
        // 取得對應語系的內容
        Map<String, String> messageMap = (Map<String, String>) codeMap.get(language);
        // 取得文字
        return ObjectUtils.defaultIfNull(messageMap.get(type), Global.Error.UNKNOWN_ERROR);
    }

    /**
     * 是否具有多國語系檔
     *
     * @return boolean
     * @author Tank.Yang
     * @since 2021-07-02 15:51:57
     */
    private boolean hasMessageFile() {
        return MapUtils.isNotEmpty(YmlUtil.getYml(Global.MultiLocale.MESSAGE_PATH));
    }

}