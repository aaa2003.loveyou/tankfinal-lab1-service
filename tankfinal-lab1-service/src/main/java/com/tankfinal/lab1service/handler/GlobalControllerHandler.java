package com.tankfinal.lab1service.handler;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.tankfinal.lab.core.constant.Global;
import com.tankfinal.lab.core.constant.SystemConstant;
import com.tankfinal.lab.core.result.Result;
//import groovy.util.logging.Slf4j;
import lombok.SneakyThrows;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Date;
import java.util.Objects;

/**
 * 全局Controller控制器
 *
 * @author Tank.Yang
 * @since 2022-04-28 10:09:22
 */
//@RestControllerAdvice(basePackages = "com.tankfinal.lab1service.controller")
//@Slf4j
//public class GlobalControllerHandler implements ResponseBodyAdvice<Object> {
public class GlobalControllerHandler {
//    @Override
//    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
//        return true;
//    }
//
//    @SneakyThrows
//    @Override
//    public Object beforeBodyWrite(Object body, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
//        Result.ResultBuilder<Object> resultBuilder = Result.builder()
//                .code(Global.SUCCESS)
//                .url(serverHttpRequest.getURI().toString())
//                .date(new Date())
//                .message(Global.SUCCESS_MSG);
//        if (serverHttpRequest.getHeaders().containsKey(SystemConstant.Feign.Header.FEIGN_TRACE_ID) || body instanceof Result) {
//            return body;
//        }
//        return resultBuilder.data(body).build();
//    }
}