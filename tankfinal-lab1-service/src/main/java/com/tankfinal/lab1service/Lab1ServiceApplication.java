package com.tankfinal.lab1service;

import ch.qos.logback.classic.LoggerContext;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.annotation.PreDestroy;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * Lab1 Service啟動類
 *
 * @author Tank.Yang
 * @since 2020-06-15 13:15:40
 */
@Slf4j
@MapperScan(basePackages = {"com.tankfinal.lab1service.mybatis.mapper", "com.tankfinal.lab1service.mybatis.mapper.test"})
@ComponentScan(basePackages = {"com.tankfinal"})
@EnableDiscoveryClient
@EnableFeignClients
@EnableHystrix
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@SpringBootApplication
@EnableAsync
public class Lab1ServiceApplication {

    /**
     * Lab1 Service 啟動入口
     *
     * @param args String[]
     * @throws UnknownHostException 取得本地ip時拋出的異常
     * @author Tank.Yang
     * @since 2020-06-15 13:16:07
     */
    public static void main(String[] args) throws UnknownHostException {
        long startTime = System.currentTimeMillis();
        log.info("=========================== 啟動 Lab1 Service ===========================");
        log.info("args:{}", JSONUtil.toJsonStr(args));
        SpringApplication application = new SpringApplication(Lab1ServiceApplication.class);
        Properties properties = new Properties();
        // 關閉無用打印
        properties.setProperty("logging.pattern.console", "");
        application.setDefaultProperties(properties);

        // 取得環境相關資訊
        ConfigurableApplicationContext configurableApplicationContext = application.run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        port = port == null ? "8080" : port;
        String eurekaPath = env.getProperty("tankfinal.cloud.eureka.ip");
        String applicationName = env.getProperty("spring.application.name");
        String datasourceUrl = env.getProperty("spring.datasource.url");
        String username = env.getProperty("spring.datasource.username");
        String password = env.getProperty("spring.datasource.password");
        log.info("=========================== 啟動完成 耗时:{} ===========================\n"
                        + "\n\t服務名稱: {}\n"
                        + "\t外部訪問地址: http://{}:{}/\n"
                        + "\tSwagger官方文檔: http://{}:{}/swagger-ui/\n"
                        + "\tKnife4j文檔: http://{}:{}/doc.html\n"
                        + "\tEureka訪問位置: http://{}:8761/\n"
                        + "\tDatasource位置: {}\n"
                        + "\tDatasource使用者資訊: {}:{}\n`",
                (System.currentTimeMillis() - startTime), applicationName, ip, port, ip, port, ip, port, eurekaPath, datasourceUrl, username, password);

    }

    /**
     * 移除LOG配置，避免實體檔案被咬住
     *
     * @author Tank.Yang
     * @since 2022-01-22 17:06:08
     */
    @PreDestroy
    public void flushLogs() {
        log.info("===== Shutdown Logger Context =====");
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        loggerContext.stop();
    }
}
