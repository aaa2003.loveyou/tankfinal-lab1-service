

CREATE TABLE PERSONAL_DATA (
                               ID INT AUTO_INCREMENT PRIMARY KEY,
                               NAME VARCHAR(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               AGE INT DEFAULT NULL,
                               VALID_FLAG INT DEFAULT NULL,
                               CREATEDATE DATETIME DEFAULT NULL,
                               CREATEID VARCHAR(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               UPDATEDATE DATETIME DEFAULT NULL,
                               UPDATEID VARCHAR(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
);

CREATE TABLE PERSONAL_DATA_DETAIL (
                                      ID INT AUTO_INCREMENT PRIMARY KEY,
                                      PERSONAL_DATA_ID INT NOT NULL,
                                      ADDRESS VARCHAR(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      VALID_FLAG INT DEFAULT NULL,
                                      CREATEDATE DATETIME DEFAULT NULL,
                                      CREATEID VARCHAR(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      UPDATEDATE DATETIME DEFAULT NULL,
                                      UPDATEID VARCHAR(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                                      CONSTRAINT FK_PERSONAL_DATA_DETAIL_PERSONAL_DATA
                                          FOREIGN KEY (PERSONAL_DATA_ID)
                                              REFERENCES PERSONAL_DATA(ID)
);

