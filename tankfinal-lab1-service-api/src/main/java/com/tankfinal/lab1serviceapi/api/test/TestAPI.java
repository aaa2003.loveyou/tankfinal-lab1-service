package com.tankfinal.lab1serviceapi.api.test;


import com.tankfinal.lab.core.result.Result;
import com.tankfinal.lab1serviceapi.components.DataResultPager;
import com.tankfinal.lab1serviceapi.components.PaginationVO;
import com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * 測試API
 *
 * @author Tank.Yang
 * @since 2020-06-15 16:43:56
 */
@RequestMapping("/api/apiTest")
@Api(value = "apiTest", description = "API", tags = {"apiTest"})
public interface TestAPI {

    /**
     * 各種測試
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-08-20 11:22:39
     */
    @ApiOperation(value = "各種測試")
    @GetMapping("/testAny")
    String testAny(@RequestParam(required = false) String fruit);


    /**
     * 基礎數據庫讀取範例
     *
     * @param paginationVO 分頁參數
     * @param name         姓名
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:48:31
     */
    @GetMapping("/getPersonalByName")
    @ApiOperation(value = "2.基礎數據庫讀取範例")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sort", value = "排序", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "當前頁數", dataType = "int"),
            @ApiImplicitParam(name = "size", value = "每頁大小", dataType = "int"),
            @ApiImplicitParam(name = "name", value = "姓名", dataType = "String", required = true)
    })
    Result<PersonalData> getPersonalByName(@ModelAttribute PaginationVO paginationVO, @RequestParam String name);

    /**
     * 數據庫Join範例
     *
     * @param paginationVO 分頁參數
     * @param name         姓名
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:49:54
     */
    @GetMapping("/getPersonalByNameJoined")
    @ApiOperation(value = "3.數據庫Join範例")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sort", value = "排序", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "當前頁數", dataType = "int"),
            @ApiImplicitParam(name = "size", value = "每頁大小", dataType = "int"),
            @ApiImplicitParam(name = "name", value = "姓名", dataType = "String", required = true),
    })
    DataResultPager<PersonalDataDTO> getPersonalByNameJoined(@ModelAttribute PaginationVO paginationVO, @RequestParam String name);

    /**
     * 可預期例外狀況範例
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:49:54
     */
    @GetMapping("/exceptedThrow")
    @ApiOperation(value = "4.可預期例外狀況範例")
    Result<Void> exceptedThrow();

    /**
     * 非預期例外狀況範例
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-07 13:49:54
     */
    @GetMapping("/unexpectedThrow")
    @ApiOperation(value = "5.非預期例外狀況範例")
    Result<Void> unexpectedThrow();

    /**
     * 基礎數據庫新增範例
     *
     * @param personalData 範例Entity
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-08 17:49:02
     */
    @PostMapping("/addPersonal")
    @ApiOperation(value = "基礎數據庫新增範例")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "personalData", value = "範例Entity", dataType = "PersonalData", required = true)
    })
    Result<Void> addPersonal(@RequestBody PersonalData personalData);


    /**
     * 基礎數據庫更新範例
     *
     * @param name 姓名
     * @param age  年齡
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-08 17:49:04
     */
    @GetMapping("/updatePersonal")
    @ApiOperation(value = "7.基礎數據庫更新範例(更新年齡)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名(where條件)", dataType = "String", required = true),
            @ApiImplicitParam(name = "age", value = "年齡", dataType = "int", required = true)
    })
    Result<Void> updatePersonal(@RequestParam String name, @RequestParam Integer age);

    /**
     * 基礎數據庫刪除範例
     *
     * @param id 鍵值
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-09-08 17:49:02
     */
    @GetMapping("/deletePersonal")
    @ApiOperation(value = "8.基礎數據庫刪除範例")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "鍵值", dataType = "int", required = true)
    })
    Result<Void> deletePersonal(@RequestParam Integer id);

    /**
     * 測試MQ推送
     *
     * @param msg 欲推送的消息
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-10-12 11:24:02
     */
    @GetMapping("/testRabbitPush")
    @ApiOperation(value = "RabbitMQ測試推送")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "msg", value = "推送的數據", dataType = "String", required = true)
    })
    Result<Void> testRabbitPush(@RequestParam String msg);

    /**
     * 測試觸發RM RabbitMQ
     *
     * @param taskSectionMainIdSet 主工段ID
     * @return com.tankfinal.lab.core.result.Result<java.lang.Void>
     * @author Vic
     * @since 2022-03-03 14:39:41
     */
    @GetMapping("/testTriggerRmTask")
    @ApiOperation(value = "測試觸發RM RabbitMQ")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskSectionMainIdSet", value = "推送的數據", dataType = "int", required = true, allowMultiple = true)
    })
    Result<Void> testTriggerRmTask(@RequestParam Set<Integer> taskSectionMainIdSet);

}
