package com.tankfinal.lab1serviceapi.api.test;


import com.tankfinal.lab.core.result.Result;
import com.tankfinal.lab1serviceapi.components.DataResultPager;
import com.tankfinal.lab1serviceapi.components.PaginationVO;
import com.tankfinal.lab1serviceapi.dto.test.PersonalDataDTO;
import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * 測試API
 *
 * @author Tank.Yang
 * @since 2020-06-15 16:43:56
 */
@RequestMapping("/api/apiTest2")
@Api(value = "apiTest", description = "API", tags = {"apiTest"})
public interface Test2API {

    /**
     * 各種測試2
     *
     * @return com.tankfinal.lab.core.result.Result
     * @author Tank.Yang
     * @since 2020-08-20 11:22:39
     */
    @ApiOperation(value = "各種測試2")
    @GetMapping("/testAny")
    String testAny2(@RequestParam(required = false) String fruit);

}
