package com.tankfinal.lab1serviceapi.dto.test;

import com.tankfinal.lab1serviceapi.model.test.PersonalData;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PersonalDataDTO extends PersonalData {
    String address;
}
