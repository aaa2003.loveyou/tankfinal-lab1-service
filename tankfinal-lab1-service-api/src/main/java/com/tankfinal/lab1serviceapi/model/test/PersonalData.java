package com.tankfinal.lab1serviceapi.model.test;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * <p>
 * 範例Entity
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@TableName("PERSONAL_DATA")
@ApiModel(description = "範例Entity")
public class PersonalData implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "id", required = false)
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @TableField("NAME")
    private String name;

    @ApiModelProperty(value = "年紀", required = true)
    private Integer age;

    @ApiModelProperty(value = "是否有效", required = false)
    private Integer validFlag;

    // 使用字段填充處理器
    private LocalDateTime createdate;

    private String createid;

    private LocalDateTime updatedate;

    private String updateid;

    public static final String ID = "ID";

    public static final String NAME = "NAME";

    public static final String AGE = "AGE";

    public static final String CREATEDATE = "CREATEDATE";

    public static final String CREATEID = "CREATEID";

    public static final String UPDATEDATE = "UPDATEDATE";

    public static final String UPDATEID = "UPDATEID";

}
