package com.tankfinal.lab1serviceapi.model.test;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author Tank.Yang
 * @since 2020-09-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("PERSONAL_DATA_DETAIL")
public class PersonalDataDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @TableField("PERSONAL_DATA_ID")
    private Integer personalDataId;

    @TableField("ADDRESS")
    private String address;

    @TableField(value = "CREATEDATE")
    private LocalDateTime createdate;

    @TableField(value = "CREATEID")
    private String createid;

    @TableField(value = "UPDATEDATE")
    private LocalDateTime updatedate;

    @TableField(value = "UPDATEID")
    private String updateid;


    public static final String ID = "ID";

    public static final String PERSONAL_DATA_ID = "PERSONAL_DATA_ID";

    public static final String ADDRESS = "ADDRESS";


    public static final String CREATEDATE = "CREATEDATE";

    public static final String CREATEID = "CREATEID";

    public static final String UPDATEDATE = "UPDATEDATE";

    public static final String UPDATEID = "UPDATEID";

}
