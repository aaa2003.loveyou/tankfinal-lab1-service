package com.tankfinal.lab1serviceapi.components;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * 簡單分類模型客製化
 * @author Tank.Yang
 * @since 2020-09-01 18:10:44
 */
public class IPageCustomImpl<T> extends Page<T> implements IPageCustom<T> {

    public IPageCustomImpl(PaginationVO dataTablePageVO){
        this(dataTablePageVO, true);
    }

    /**
     * 設定排序判斷規則，目前僅支援單欄位排序
     * @author Tank.Yang
     * @since 2020-09-07 10:46:49
     * @param paginationVO 分頁參數
     * @param backEndSort 後端分頁
     */

    public IPageCustomImpl(PaginationVO paginationVO, boolean backEndSort) {
        if (paginationVO != null) {
            if (paginationVO.getCurrent() > 1) {
                this.setCurrent(paginationVO.getCurrent());
            }
            this.setSize(paginationVO.getSize());
            this.setSearchCount(true);
            if(backEndSort){
                if(StringUtils.isNotEmpty(paginationVO.getOrderByColumn()) &&
                        StringUtils.isNotEmpty(paginationVO.getOrderByMode())){
                    String order = paginationVO.getOrderByMode();
                    List<OrderItem> list = new ArrayList<>();
                    if(order.compareTo(DataResultPager.DESC) == 0){
                        list.add(OrderItem.desc(paginationVO.getOrderByColumn()));
                    } else if (order.compareTo(DataResultPager.ASC) == 0) {
                        list.add(OrderItem.asc(paginationVO.getOrderByColumn()));
                    }
                    this.setOrders(list);
                }
            }
        }
    }

    @Override
    public IPageCustomImpl<T> setRecords(List<T> records) {
        super.setRecords(records);
        return this;
    }

    @Override
    public IPageCustomImpl<T> setTotal(long total) {
        super.setTotal(total);
        return this;
    }

    @Override
    public IPageCustomImpl<T> setSize(long size) {
        super.setSize(size);
        return this;
    }

    @Override
    public IPageCustomImpl<T> setCurrent(long current) {
        super.setCurrent(current);
        return this;
    }

}
