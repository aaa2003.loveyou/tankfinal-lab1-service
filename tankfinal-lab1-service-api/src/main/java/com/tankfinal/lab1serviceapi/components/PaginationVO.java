package com.tankfinal.lab1serviceapi.components;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * 客製化分頁參數
 * @author Tank.Yang
 * @since 2020-09-01 16:39:10
 */
@Data
@Slf4j
@ApiModel(description = "分頁參數容器")
public class PaginationVO implements Serializable {

    private static final long serialVersionUID = -5787652995241127057L;

    @ApiModelProperty(value = "排序", required = false)
    private String sort;

    @ApiModelProperty(value = "第幾頁", required = false)
    private Integer page;

    @ApiModelProperty(value = "每頁顯示幾筆", required = false)
    private Integer size;

    /**
     * 取得當前頁數，預設10
     * @author Tank.Yang
     * @since 2020-09-07 10:37:37
     * @return int
     */
    public int getSize() {
        if(size != null && size != 0){
            try {
                return size;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return 10;
    }

    /**
     * 預設當前頁，預設1
     * @author Tank.Yang
     * @since 2020-09-07 10:37:54
     * @return int
     */
    public int getCurrent() {
        if(page != null && page != 0){
            try {
                return page;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return 1;
    }

    /**
     * 排序參數客製化
     * @author Tank.Yang
     * @since 2020-09-07 10:43:22
     * @return java.lang.String
     */
    public String getOrderByMode() {
        if(StringUtils.isNotBlank(sort) && sort.split(DataResultPager.COMMA).length > 1){
            String order = sort.toLowerCase().split(DataResultPager.COMMA)[1].trim();
            if(DataResultPager.DESC.equals(order)){
                return DataResultPager.DESC;
            } else if (DataResultPager.ASC.equals(order)) {
                return DataResultPager.ASC;
            }
        }
        return "asc";
    }
    /**
     * 排序參數客製化
     * @author Tank.Yang
     * @since 2020-09-07 10:43:22
     * @return java.lang.String
     */
    public String getOrderByColumn() {
        if(StringUtils.isNotBlank(sort) && sort.split(DataResultPager.COMMA).length > 1){
            return sort.toUpperCase().split(DataResultPager.COMMA)[0].trim();
        }
        return "1";
    }
}
