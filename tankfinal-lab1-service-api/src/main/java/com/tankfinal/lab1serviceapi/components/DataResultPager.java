package com.tankfinal.lab1serviceapi.components;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * DataResultPager分頁封裝結果
 * @author Tank.Yang
 * @since 2020-09-04 18:39:12
 */
@Getter
@Setter
@ApiModel(description = "回傳分頁封裝容器")
public class DataResultPager<T> {

    /**
     * 當前第幾頁
     */
    @ApiModelProperty(value = "當前第幾頁", required = false)
    private long number;

    /**
     * 每頁顯示數量
     */
    @ApiModelProperty(value = "每頁顯示數量", required = false)
    private long size;

    /**
     * 資料總數
     */
    @ApiModelProperty(value = "資料總數", required = false)
    private long totalElements;

    /**
     * 總頁數
     * */
    @ApiModelProperty(value = "總頁數", required = false)
    private long totalPages;

    /**
     * 是否為第一頁
     * */
    @ApiModelProperty(value = "是否為第一頁", required = false)
    private Boolean first;

    /**
     * 是否
     * */
    @ApiModelProperty(value = "是否為最後一頁", required = false)
    private Boolean last;

    /**
     * 數據列表
     */
    @ApiModelProperty(value = "數據列表", required = false)
    private List<T> content;

    public DataResultPager(IPageCustom<T> customPage) {
        this.content = customPage.getRecords();
        this.size = customPage.getSize();
        this.totalPages = customPage.getPages();
        this.number = customPage.getCurrent();
        this.totalElements = customPage.getTotal();
        this.last = this.totalPages == this.number;
        this.first = this.number < 2;
    }
    public static final String ASC = "asc";
    public static final String DESC = "desc";
    public static final String COMMA = ",";
}
