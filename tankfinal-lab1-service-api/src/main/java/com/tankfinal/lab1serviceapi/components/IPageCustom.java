package com.tankfinal.lab1serviceapi.components;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 客制化分頁 CustomPage 對象接口
 * @author Tank.Yang
 * @since 2020-09-04 18:38:16
 */
public interface IPageCustom<T> extends IPage<T> {

    /**
     * 處理反序列報錯問題
     * @author Tank.Yang
     * @since 2020-09-04 18:37:59
     * @param pages Pages
     * @return com.tankfinal.apsserviceapi.components.IPageCustom<T>
     */
    @Override
    default IPageCustom<T> setPages(long pages) {
        // to do nothing
        return this;
    }

    /**
     * 設置分頁紀錄列表
     * @author Tank.Yang
     * @since 2020-09-04 18:38:26
     * @param records
     * @return com.tankfinal.apsserviceapi.components.IPageCustom<T>
     */
    @Override
    IPageCustom<T> setRecords(List<T> records);

    /**
     * 設置當前滿足條件總行數
     * @author Tank.Yang
     * @since 2020-09-04 18:38:42
     * @param total
     * @return com.tankfinal.apsserviceapi.components.IPageCustom<T>
     */
    @Override
    IPageCustom<T> setTotal(long total);

    /**
     * 設置當前分頁總數
     * @author Tank.Yang
     * @since 2020-09-04 18:38:53
     * @param size
     * @return com.tankfinal.apsserviceapi.components.IPageCustom<T>
     */
    @Override
    IPageCustom<T> setSize(long size);

    /**
     * 設置當前頁
     * @author Tank.Yang
     * @since 2020-09-04 18:39:04
     * @param current
     * @return com.tankfinal.apsserviceapi.components.IPageCustom<T>
     */
    @Override
    IPageCustom<T> setCurrent(long current);

}
